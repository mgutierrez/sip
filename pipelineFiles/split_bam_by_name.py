'''
Created on 06/04/2014

@author: antonior
'''

import pysam
import json
import optparse
import os

def split_by_name(filename,outdir,json_sms_file):
 
    infile = pysam.Samfile(filename,'rb')
    
    SMs=json.load(file(json_sms_file))
    header = infile.header
    
    readgroups = header['RG']
    # remove readgroups from header
    del(header['RG'])
    outfiles = {}
    out_files={}
    for rg in readgroups:
        tmphead = header
        tmphead['RG']=[rg]
        outfilename=outdir+"/"+os.path.basename(SMs[rg['SM']][0].replace(".bam",".recal.realing.bam"))
        outfiles[rg['ID']] = pysam.Samfile(outfilename,'wb',header=tmphead)
        out_files[SMs[rg['SM']][1]]=outfilename
     
    for read in infile:
        idtag = [x[1] for x in read.tags if x[0]=='RG'][0]
        outfiles[idtag].write(read)
     
    for f in outfiles.values():
        f.close()
     
    infile.close()
    
    return out_files



def main():
    ################################################

    #### Options and arguments #####################

    ################################################  
            
    usage=""" 
    ************************************************************************************************************************************************************
    Task: 
    ************************************************************************************************************************************************************
    """
    parser = optparse.OptionParser(usage)
    parser.add_option("--filename", dest="filename", help="""Required. Two or more samples bam file.""",default=None)
    parser.add_option("--outdir", dest="outdir", help="""Required. Path to outdir.""",default=None)
    parser.add_option("--sms_file", dest="sms", help="""Required. Sample names and filenames in json format.""",default=None)

    
    (options, args) = parser.parse_args()
    
    split_by_name(options.filename,options.outdir,options.sms)
    
    




if __name__ == '__main__':
    main()
