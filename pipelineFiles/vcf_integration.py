#!/usr/bin/python

""" This python script takes one or more vcf files from different callers and combines them into a normalized vcf file (GBPA standard)
Each variant of the resulting vcf file follows this syntax:

#CHROM	        POS	        ID	    REF	            ALT	            QUAL	    FILTER	    INFO	FORMAT	    NORMAL	            TUMOR
<chromosome>    <position>  <id>    <reference>     <alternative>   <quality>   <filter>    <info>  <format>    <normal_sample>     <tumor_sample>

Fields:
1  - chromosome: original chromosome value from callers
2  - position: position within the chromosome
3  - id: variant identifier. The value is any of the 'id' of the combined variants, different of '.' if possible
4  - reference: reference nucleotide
5  - reference: alternative nucleotide (concatenation of all callers)
6  - quality: max quality value from all the given callers (or '.' character if not available)
7  - filter: concatenated (with ';' character) filter values from all callers
8  - info: concatenated (with ';' character) info values from all the callers
9  - format         |\
10 - normal_sample  |-------> format and info about genotype information, both normal and tumor samples; combined from all the callers
11 - tumor_sample   |/
"""

from optparse import OptionParser
import sys, os
sys.path.insert(1, os.path.join(sys.path[0], ".."))
import lib.standard_vcf as stdvcf
import GbpaCommonLibs.FileModels.vcf_file as vcf_file


def parse_options():
    parser = OptionParser('usage: %prog -v <vcfs> -c <callers> [-o <outfile>]')
    parser.add_option('-v', '--vcfs', dest='vcfs',
                      help='the vcfs files (comma separated, without space)')
    parser.add_option('-c', '--callers', dest='callers',
                      help='sorted list of callers for each vcf (comma separated, without space)')
    parser.add_option('-o', '--outfile', dest='outfile',
                      help='outfile.vcf')
    return parser


def writeMetainfoAndHeader(fd, vcfCallerObjects):
    colnames = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT', 'NORMAL', 'TUMOR']

    callers = {}
    for caller in vcfCallerObjects:
        name=caller.__class__.__name__.split("_")[1]
        prefix=caller.prefix
        callers[prefix]=name

    fd.write("##fileformat=VCFv4.1\n")
    fd.write("##prefix=%s\n" % ','.join(map(lambda x:'='.join(x),callers.items())))
    fd.write("#%s\n" % '\t'.join(colnames))

    return


def set_parameters():
    parser  = parse_options()
    options = parser.parse_args()[0]

    if options.vcfs is None:
        parser.error('at least one vcf file is needed')
    else:
        vcfs = options.vcfs.split(',')

    if options.callers is None:
        parser.error('at least one caller is needed')
    else:
        callers = options.callers.split(',')

    if len(callers) != len(vcfs):
        parser.error("Callers' length doesn't match with vcf files")

    outfile = options.outfile

    # check if vcfs exist
    vcfs_list = []
    callers_list = []

    for vcf, caller in zip(vcfs, callers):
        if os.path.exists(vcf):
            vcfs_list.append(vcf)
            callers_list.append(caller)
            print "Found vcf file '%s'" % vcf
        else:
           print "Ignoring non existent vcf file '%s'" % vcf

    if vcfs_list == list():
        sys.stderr.write("WARNING: no vcf files to integrate. Exiting normally\n")
        sys.exit(0)

    for caller, vcf in zip(callers_list, vcfs_list):
        try:
            eval("stdvcf.VcfFromCaller_%s('%s')" % (caller, vcf))
        except AttributeError:
            sys.stderr.write("\nERROR: class stdvcf.VcfFromCaller_%s does not exist!!!\n" % caller)
            sys.exit(1)

    return vcfs_list, callers_list, outfile




def main():
    "The real script"
    vcfs_list, callers_list, outfile = set_parameters()

    try:
        outFileDescriptor = sys.stdout if outfile is None else file(outfile,'w')
        somatizedOutFileDescriptor = sys.stdout if outfile is None else file(os.path.splitext(outfile)[0] + os.path.splitext(outfile)[1].replace(".vcf", "")+".somatized.vcf", "w")
    except IOError as e:
        sys.stderr.write("Error: cannot write to '%s': permission denied!\n" % e.filename)
        sys.exit(1)

    vcfCallerObjects=[]
    for caller,vcf in zip(callers_list,vcfs_list):
        vcfCallerObjects.append(eval("stdvcf.VcfFromCaller_%s('%s')" % (caller, vcf)))

    if outFileDescriptor != sys.stdout: writeMetainfoAndHeader(outFileDescriptor, vcfCallerObjects)
    writeMetainfoAndHeader(somatizedOutFileDescriptor, vcfCallerObjects)

    # main loop
    mainCaller=vcfCallerObjects.pop(0)
    for variant in mainCaller.combine(vcfCallerObjects):
        if outFileDescriptor != sys.stdout: outFileDescriptor.write(str(variant)+'\n')
        # we have a normalized and combined variant. Let's apply some more changes. Please refer to 'somatizeMe' method for more info
        if not variant.somatizeMe(): raise StandardError("Error somatizing variant!!!")
        somatizedOutFileDescriptor.write(str(variant)+"\n")

    outFileDescriptor.close()
    somatizedOutFileDescriptor.close()

    return



if __name__ == '__main__':
    main()
