'''
Created on 24/03/2014

@author: antonior
'''
import sys, os
sys.path.insert(1, os.path.join(sys.path[0], ".."))
import argparse
import GbpaCommonLibs.FileModels.vcf_file as vcf_file

def _get_header_lines(vcf):
    'It gets an iterator with header lines and colnames'
    for line in open(vcf.filename):
        if line.startswith('#'):
            yield line
        else:
            break

def _validVcfList(vcfs):
    for f in vcfs.split(","):
        if not os.path.isfile(f):
            raise argparse.ArgumentTypeError("ERROR: no such file '%s'" % f)
    return vcfs.split(",")


def main():
    usage="""
    ************************************************************************************************************************************************************
    Task: 
    ************************************************************************************************************************************************************
    """
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument("--vcfs", dest="vcfs", help="Required. List of comma separated vcf files to join", required=True, type=_validVcfList)
    parser.add_argument("--outfile", dest="outfile", help="Required. Out vcf file", required=True)

    options = parser.parse_args()
    vcf_file.vcf_file.concatSortedVcfs(options.vcfs, options.outfile)


if __name__ == '__main__':
    main()