import sys,os,logging

from pipelineFiles.calling import pipeline as calling
from lib.utils import make_dir
from lib.cmd_utils import create_qsub_cmd, runcmd
import pipelineFiles.pair_bam_preprocessing as pair_bam_preprocessing
import json
import settings

logger = logging.getLogger('root')

class WorkingDir:
    ''' This class implements either a working or output directory structure and functions
    '''
    def __init__(self, path):
        self.path = path
        self.calling_dir = os.path.join(self.path, '2_Variant_Calling')

    @property
    def get_log_dir(self):
        """
        retrieves the log_dir path and creates it if it does not exist
        :return: the log directory name
        """
        log_dir = os.path.join(self.path, '0_log')
        make_dir(log_dir)
        return log_dir

    @property
    def get_integration_dir(self):
        """
        retrieves the integration_dir path and creates it if it does not exist
        :return: integration_dir name
        """
        int_dir = os.path.join(self.path, '3_Vcf_Integration')
        make_dir(int_dir)
        return int_dir
    
    @property
    def get_filtration_dir(self):
        """
        it retrieves the filtration_dir path and creates it if it does not exist
        :return: filtration_dir name
        """
        int_dir = os.path.join(self.path, '4_Vcf_Filtration')
        make_dir(int_dir)
        return int_dir

    @property
    def get_preprocess_dir(self):
        """
        it retrieves the preprocess_dir path and creates it if it does not exist
        :return: preprocess_dir name
        """
        int_dir = os.path.join(self.path, '1_preprocess')
        make_dir(int_dir)
        return int_dir


class pipeline:
    ''' This class implements the pipeline itself
    '''
    def __init__(self, options):
        self.options = options
        self._get_status_and_variant_list()

    def _get_status_and_variant_list(self):
        """
        sets the somatic status attribute and type of variant
        :return: nothing
        """
        if self.options['status'] == 'all':
            self.status_list = ['som', 'loh', 'germ']
        else:
            self.status_list = [self.options['status']]
        self.variant_list = ['snv', 'indel']

        return

    def _savePipelineTrace(self, data, append=True):
        """
        Save pipeline trace (data) to out+"pipeline.trace.json"
        :param data: data to save into file. Normally is a dict with the form {"pipelineStep":{dict_with_data}}
        :param append: append to existing file if true
        """
        logger.info("Saving pipeline trace")
        filePath = os.path.join(self.options['out'], settings.PIPELINE_TRACE_FILENAME)
        jsonData = {}
        if append:
            # load previous content. If file does not exist, ignore silently (will be written later)
            try:
                with open(filePath, "r") as fd:
                    jsonData = json.load(fd)
            except IOError:
                pass
        try:
            with open(filePath, "w") as fd:
                json.dump(dict(jsonData, **data), fd)
        except Exception as e:
            logger.critical("Error loading pipeline trace file! %s" % filePath)
            raise e

        logger.debug("Pipeline trace saved to '%s': %s" % (os.path.join(self.options['workingdir'], settings.PIPELINE_TRACE_FILENAME), str(dict(jsonData, **data))))
        return


    def _loadPipelineTrace(self, pipelineTrace):
        """
        Load pipeline trace (data) from PIPELINE_TRACE_FILENAME
        :param pipelineTrace: if set, does not load anything from disk. Instead returns the same pipelineTrace (it comes from a previous step)
        :return: pipeline trace, from disk or previous step
        """
        logger.info("Loading pipeline trace")

        if pipelineTrace is None:
            # load pipeline trace from disk
            filePath = os.path.join(self.options['workingdir'], settings.PIPELINE_TRACE_FILENAME)
            try:
                with open(filePath, "r") as fd:
                    jsonData = json.load(fd)
            except Exception as e:
                logger.critical("Error loading pipeline trace file! %s" % filePath)
                raise e
            ret = jsonData
            logger.debug("Pipeline trace loaded from file '%s': %s" % (str(filePath), str(jsonData)))
        else:
            # else, load from recently executed previous step
            ret = pipelineTrace
            logger.debug("Pipeline loaded from previous step: %s" % str(pipelineTrace))

        return ret


    def run_pair_bam_preprocessing(self):
        """
        pipeline step 1: preprocessing
        :return: pipeline trace for next step
        """
        logger.info("-- MARK --")
        logger.info(" ** Starting preprocessing step **")
        wdir = WorkingDir(self.options['workingdir'])
        outdir = wdir.get_preprocess_dir
        logdir = wdir.get_log_dir

        processfiles = pair_bam_preprocessing.run_preprocess_bams(self.options['tumor'], self.options['normal'], self.options['prefix'], self.options['reference'],outdir, log_dir=logdir, qsub=self.options['qsub'])

        self.options['normal']=processfiles['normal']
        self.options['tumor']=processfiles['tumor']

        # save pipeline trace
        data = {'preprocessing': {'normal':processfiles['normal'], 'tumor': processfiles['tumor']}}
        self._savePipelineTrace(data, append = False)

        logger.info("** Finished preprocessing step **")
        return data


    def run_calling_mode(self, pipelineTrace):
        """
        pipeline step 2: calling
        :param pipelineTrace: pipeline trace from previous step
        :return: updated pipeline trace for next step
        """
        logger.info("-- MARK --")
        logger.info(" ** Starting calling step **")
        hold = self.options['prefix']+'.splitbyname' if self.options['mode'] != "calling" else str(-1)
        jsonData = self._loadPipelineTrace(pipelineTrace)

        self.options['normal'] = jsonData["preprocessing"]["normal"]
        self.options['tumor'] = jsonData["preprocessing"]["tumor"]
        logger.info("Normal bam: %s" % self.options['normal'])
        logger.info("Tumor bam: %s" % self.options['tumor'])
        logger.debug("hold_jid = %s" % str(hold))

        multicalling = calling(self.options['normal'], self.options['tumor'],
                                   self.options['reference'], self.options['out'],
                                   self.options['callers'], self.options['prefix'],
                                   'all', self.options['qsub'],hold_jid=hold)
        multicalling.run()

        # save pipeline trace
        data = {'calling': {'callersList': multicalling.callers, 'prefix': multicalling.prefix, 'dir': multicalling.calling_dir}}
        self._savePipelineTrace(dict(jsonData, **data), append=True)

        logger.info("** Finished calling step **")
        return dict(jsonData, **data)


    def run_integration_mode(self, pipelineTrace):
        """
        pipeline step 3: integration
        :param pipelineTrace: pipeline trace from previous step
        :return:
        """
        logger.info("-- MARK --")
        logger.info(" ** Starting integration step **")
        jsonData = self._loadPipelineTrace(pipelineTrace)


        vcfsCreatedList = {}
        for variant in self.variant_list:
            logger.info("Running integration for '%s' variant" % str(variant))
            vcfs_list = []
            callers_list = []
            if set(self.options['callers']) - set(jsonData['calling']['callersList']) != set():
                logger.warning("Ignoring non existent callers (but specified in options) in pipeline trace: %s" % str([ f for f in set(self.options['callers']) - set(jsonData['calling']['callersList']) ]))
            for caller in set(jsonData['calling']['callersList']).intersection(set(self.options['callers'])):
                for ss in settings.SUPPORTED_STATUS:
                    vcf = os.path.join(jsonData['calling']['dir'], caller, jsonData['calling']['prefix'] + "." + caller + "." + variant + "." + ss + ".raw.vcf")
                    vcfs_list.append(vcf)
                    callers_list.append(caller)
            vcfsCreatedList[variant] = self.get_standard_vcf(vcfs_list, callers_list, self.options['prefix'], self.options['out'], variant)


        if vcfsCreatedList == dict():
            logger.critical("No integration output files! Aborting.")
            sys.exit(1)
        logger.debug("Created vcf files: '%s'" % str(vcfsCreatedList))
        # save pipeline trace
        data = {'integration': vcfsCreatedList}
        self._savePipelineTrace(dict(jsonData, **data), append=True)
        logger.info("** Finished integration step **")

        return dict(jsonData, **data)


    def run_filtration_mode(self, pipelineTrace):
        """
        pipeline step 4: filtration
        :param pipelineTrace: pipeline trace from previous step
        :return:
        """
        logger.info("-- MARK --")
        logger.info(" ** Starting filtration step **")
        odir = WorkingDir(self.options['out'])
        jsonData = self._loadPipelineTrace(pipelineTrace)
        hold_jid = self.options['prefix'] + '.integration'
        job_id = self.options['prefix'] + '.filtration'
        log_dir = odir.get_log_dir
        outdir = odir.get_filtration_dir

        path_to_sh = os.path.join(sys.path[0], "bash/do_filtration.sh")
        path_to_py = os.path.join(sys.path[0], "pipelineFiles/vcf_file_filter.py")

        logger.debug("hold_jid = %s" % str(hold_jid))
        logger.debug("job_id = %s" % str(job_id))
        logger.debug("log directory: %s" % log_dir)
        logger.debug("Filtration out directory: %s" % str(outdir))
        logger.debug("Bash script to execute: %s" % str(path_to_sh))
        logger.debug("Python script to execute: %s" % str(path_to_py))

        vcfsCreatedDict = {}
        for variant in self.variant_list:
            vcfsCreatedDict[variant] = {}
            for ss in self.status_list:
                logger.info("Running filtration for '%s' variant and '%s' status" % (str(variant), str(ss)))
                if variant in jsonData["integration"].keys():
                    vcf = jsonData["integration"][variant]
                    logger.debug("Vcf file is '%s'" % str(vcf))
                    if vcf != "":
                        outfile = os.path.join(outdir, self.options['prefix']+"."+variant+"."+ss+".standard.somatized.vcf")
                        logger.debug("Out file is '%s'" % outfile)
                        command = ' '.join(['', path_to_sh, vcf, outfile, ss, str(self.options['minPCOUNT']), path_to_py, sys.path[0]])
                        cmd = create_qsub_cmd(command, job_id, hold_jid=hold_jid,path_stdout=log_dir, path_stderr=log_dir, h_vmem='2G', pe_name=settings.PE_NAME, ncores=2) if self.options['qsub'] else command
                        runcmd(cmd)
                        vcfsCreatedDict[variant][ss] = outfile
                logger.info("Filtration for '%s' variant and '%s' status has finished" % (str(variant), str(ss)))

        # join files by somatic status if specified in options
        if self.options['joinvariant']:
            logger.info("Joining vcf files with same somatic status per variant")
            for ss in self.status_list:
                vcfs = list()
                for variant in self.variant_list:
                    vcfs.append(vcfsCreatedDict[variant][ss])
                logger.debug("Vcf files to join: %s" % vcfs)
                outfile = os.path.join(outdir, self.options['prefix']+".all."+ss+".standard.somatized.vcf")
                logger.debug("Out vcf file: %s" % outfile)
                path_to_sh = os.path.join(sys.path[0], "bash/do_joinvariant.sh")
                path_to_py = os.path.join(sys.path[0], "pipelineFiles/joinvariant.py")
                hold_jid = self.options['prefix'] + '.filtration'
                job_id = self.options['prefix'] + '.joinvariant'
                logger.debug("hold_jid = %s" % str(hold_jid))
                logger.debug("job_id = %s" % str(job_id))
                command = ' '.join([path_to_sh, path_to_py, ','.join(vcfs), outfile, sys.path[0]])
                cmd = create_qsub_cmd(command, job_id, hold_jid=hold_jid, path_stdout=log_dir, path_stderr=log_dir, h_vmem='2G', pe_name=settings.PE_NAME, ncores=6) if self.options['qsub'] else command
                runcmd(cmd)
            logger.info("Joining finished")

        logger.debug("Created vcf files: '%s'" % str(vcfsCreatedDict))
        # save pipeline trace
        data = {'filtration': vcfsCreatedDict}
        self._savePipelineTrace(dict(jsonData, **data), append=True)

        logger.info("** Finished filtration step **")

        return dict(jsonData, **data)


    def get_standard_vcf(self, vcf_list, callers_list, prefix, out, variant):
        """
        integrates the outputs from the different callers making only one standard vcf file. The filename ends with ".somatized.vcf" string
        :param vcf_list: vcfs list to join
        :param callers_list: list of callers
        :param prefix: prefix for output file
        :param out: output directory to save the vcf file
        :param variant: take only a certain type of variant
        :return: returns the file name of the generated vcf
        """
        out = WorkingDir(out)
        log_dir = out.get_log_dir
        int_dir = out.get_integration_dir

        filename = '.'.join([prefix, variant, "standard.vcf"])
        filename = os.path.join(int_dir, filename)

        vcfs = ",".join(vcf_list)
        callers = ",".join(callers_list)
        hold_jid = [".".join([prefix, caller]) for caller in callers_list]

        path_to_sh = os.path.join(sys.path[0], "bash/do_integrate_callers.sh")
        path_to_py = os.path.join(sys.path[0], "pipelineFiles/vcf_integration.py")
        
        job_id = prefix + '.integration'

        logger.debug("hold_jid = %s" % str(hold_jid))
        logger.debug("job_id = %s" % str(job_id))
        logger.debug("log directory: %s" % log_dir)
        logger.debug("Integration out directory: %s" % str(int_dir))
        logger.debug("Bash script to execute: %s" % str(path_to_sh))
        logger.debug("Python script to execute: %s" % str(path_to_py))
        logger.debug("Integration input vcf files: '%s'" % str(vcf_list))

        command = ' '.join(['', path_to_sh, vcfs, callers, filename, path_to_py, sys.path[0]])
        cmd = (create_qsub_cmd(command, job_id, hold_jid=hold_jid, path_stdout=log_dir, path_stderr=log_dir, h_vmem='2G', pe_name=settings.PE_NAME, ncores=6) \
            if self.options['mode'] != "integration" else create_qsub_cmd(command, job_id, path_stdout=log_dir, path_stderr=log_dir, h_vmem='2G', pe_name=settings.PE_NAME, ncores=6)) if self.options['qsub'] else command

        runcmd(cmd)
        logger.info("Integrated and somatized output vcf file (if created): '%s'" % str(os.path.splitext(filename)[0] + os.path.splitext(filename)[1].replace(".vcf", "")+".somatized.vcf"))

        # returns vcf standard (somatized) filename, created by integrator
        return os.path.splitext(filename)[0] + os.path.splitext(filename)[1].replace(".vcf", "")+".somatized.vcf"


    def run_full_mode(self):
        """
        run the whole pipeline from the beginning
        """
        self.options['workingdir'] = self.options['out']
        pipelineTrace = self.run_pair_bam_preprocessing()
        pipelineTrace = self.run_calling_mode(pipelineTrace)
        pipelineTrace = self.run_integration_mode(pipelineTrace)
        pipelineTrace = self.run_filtration_mode(pipelineTrace)