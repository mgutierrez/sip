'''
Created on 11/03/2014

@author: eva
@author: antonior
'''

#!/usr/bin/python

import sys,os,settings
sys.path.insert(1, os.path.join(sys.path[0], ".."))
from lib.utils import make_dir
from lib.cmd_utils import create_qsub_cmd, runcmd
import logging
logger = logging.getLogger('root')

class pipeline:

    def __init__(self, normal_bam, tumor_bam, reffile, outfolder, callers,
                 prefix, variant = 'snv', qsub=True, parameters=None,infolder=None,hold_jid=None):

        self.outfolder = outfolder
        self.callers = callers
        self.normal_bam = normal_bam
        self.tumor_bam = tumor_bam
        self.reffile = reffile
        self.calling_dir = os.path.join(self.outfolder, '2_Variant_Calling')
        self.log_dir = outfolder+"/0_log"
        self.variant = variant
        self.hold_jid = hold_jid
        self.prefix = prefix
        self.qsub = qsub


    def make_output_dirs(self):
        '''It generates a directory inside the outfolder path which contains
        a folder for each caller that will be run '''
        make_dir(self.calling_dir)
        for caller in self.callers:
            make_dir(os.path.join(self.calling_dir, caller))
            make_dir(self.log_dir)
        return

    def launchBashCaller(self, caller, hold_jid=None):
        job_id = self.prefix + "." + caller
        outdir = os.path.join(self.calling_dir, caller)
        path_to_caller_sh = os.path.join(sys.path[0], "bash", "callers", caller + ".sh")

        command = ' '.join([path_to_caller_sh, self.normal_bam, self.tumor_bam, self.reffile, self.prefix, outdir, sys.path[0]])
        cmd = (create_qsub_cmd(command = command, job_id = job_id, path_stdout=self.log_dir, path_stderr=self.log_dir, h_vmem='20G', pe_name=settings.PE_NAME, ncores=2) if hold_jid == "-1" else create_qsub_cmd(command = command, job_id = job_id, hold_jid = hold_jid, path_stdout=self.log_dir, path_stderr = self.log_dir, h_vmem='20G', pe_name=settings.PE_NAME, ncores=2)) if self.qsub else command

        logger.info("Running '%s' caller" % caller)
        runcmd(cmd)

        return


    def run(self):
        'It runs the pipeline'
        self.make_output_dirs()
        for caller in self.callers:
            self.launchBashCaller(caller, self.hold_jid)

        return
