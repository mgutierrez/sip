'''
Created on 24/03/2014

@author: antonior
'''
import sys,os
sys.path.insert(1, os.path.join(sys.path[0], ".."))
import optparse
import GbpaCommonLibs.FileModels.vcf_file as vcf_file

def check_parameters(options,parser):
    #Check required prameters
    if options.vcf==None or options.outfile==None or options.min==None or options.ss==None:
        parser.print_help()
        print 'ERROR: --vcf, --outfile, --minPCOUNT, -ss parameters are required.'
        sys.exit(1)

    if options.ss.lower() not in ["som", "loh", "germ"]:
        parser.print_help()
        parser.error("Invalid somatic status. Available values: som, log, germ")
    return True


def check_files(vcf):
    #Check required prameters
    if(not (os.path.isfile(vcf) or os.path.islink(vcf))):
        sys.stderr.write("WARNING: vcf file '%s' does not exist. Exiting normally\n" % vcf)
        sys.exit(0)
    else:
        return True


def _get_header_lines(vcf):
    'It gets an iterator with header lines and colnames'
    for line in open(vcf.filename):
        if line.startswith('#'):
            yield line
        else:
            break


def main():
    ################################################

    #### Options and arguments #####################

    ################################################  
            
    usage=""" 
    ************************************************************************************************************************************************************
    Task: 
    ************************************************************************************************************************************************************
    """
    parser = optparse.OptionParser(usage)
    parser.add_option("--vcf", dest="vcf", help="""Required. Standard vcf file to filtrate""")
    parser.add_option("--outfile", dest="outfile", help="""Required. Out vcf file""")
    parser.add_option("--minPCOUNT", dest="min", help="""variants will be labeled with PASS if they are called by at least "min" of callers""")
    parser.add_option("--ss", dest="ss", help="""Required. Somatic status filter (som, log, germ)""")
    
    (options, args) = parser.parse_args()
    if check_parameters(options,parser):
        if check_files(options.vcf):
            vcfFile=vcf_file.vcf_file(options.vcf)
            SSs={'germ': 1, 'som': 2, 'loh': 3}
            ss = options.ss
            minPCOUNT=int(options.min)

            #outFileDescriptor=file(os.path.join(os.path.dirname(options.outfile), os.path.basename(options.outfile).replace("standard.somatized",ss+".standard.somatized")), "w")
            outFileDescriptor=file(options.outfile, "w")
            for line in _get_header_lines(vcfFile):
                outFileDescriptor.write(line)
            for variant in vcfFile.get_variants():
                if (int(variant.info["PCOUNT"+str(SSs[ss])]) >= minPCOUNT):
                    outFileDescriptor.write(str(variant)+"\n")
            outFileDescriptor.close()


if __name__ == '__main__':
    main()