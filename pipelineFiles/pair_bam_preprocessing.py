''''
Created on 03/04/2014

@author: antonior
'''

import pysam,settings
import optparse
import sys,os
sys.path.insert(1, os.path.join(sys.path[0], ".."))
from lib.utils import make_dir
from lib.cmd_utils import create_qsub_cmd, runcmd
import json
import logging

logger = logging.getLogger('root')


def check_parameters(options,parser):
    #Check required prameters
    if options.tumor == None or options.normal==None or options.prefix==None or options.reference==None or options.outdir==None:
        parser.print_help()
        print 'ERROR: ---normal, --tumor, --prefix, --reference, --outdir parameters are required.'
        sys.exit(1)
    return True



def read_sample_name(filename):
    try:
        logger.debug("Opening bam file '%s'" % filename)
        sam = pysam.Samfile(filename,'rb')
        header = sam.header
        readgroups = header['RG']
        if len(readgroups) > 1: raise Exception
    except Exception as e:
        logger.critical("Unexpected error while reading bam file '%s'" % filename)
        raise e

    return readgroups[0]['SM']


def realignment(prefix, tumor, normal, reference, outdir, log_dir, qsub=False):
    """
    pipeline step 1.1: realignment
    :param tumor: tumor bam file
    :param normal: normal bam file
    :param prefix: prefix to prepend to file names
    :param reference: reference fasta file
    :param outdir: out directory to save files
    :param log_dir: log files directory (only applies to queue jobs)
    :param qsub: wheter or not send the job to a queue system (SGE)
    :return: realigned file
    """
    logger.info("Performing realignment")
    make_dir(log_dir)
    job_id = prefix + '.realignment'
    path_to_sh = os.path.join(sys.path[0], "bash/do_indel_realignment.sh")
    out_file = os.path.join(outdir, prefix+"_realignment.bam")
    target_intervals = os.path.join(outdir, prefix+"_target_intervals.list")

    logger.debug("job_id = %s" % str(job_id))
    logger.debug("Bash script to execute: %s" % path_to_sh)
    logger.debug("Target intervals file '%s'" % target_intervals)
    logger.info("Output bam file is '%s'" % out_file)

    command = ' '.join([path_to_sh,reference, tumor, normal, out_file, target_intervals, sys.path[0]])
    cmd = create_qsub_cmd(command, job_id, path_stdout=log_dir, path_stderr=log_dir, h_vmem='20G', pe_name=settings.PE_NAME, ncores=2) if qsub else command
    runcmd(cmd)

    return out_file


def baserecalibration (prefix, filename, reference, outdir, log_dir, qsub=False):
    """
    pipeline step 1.2: base recalibration
    :param filename: file name to recalibrate
    :param prefix: prefix to prepend to file names
    :param reference: reference fasta file
    :param outdir: out directory to save files
    :param log_dir: log files directory (only applies to queue jobs)
    :param qsub: wheter or not send the job to a queue system (SGE)
    :return: recalibrated file
    """
    logger.info("Performing base recalibration")
    make_dir(log_dir)
    job_id = prefix+'.recalibration'
    path_to_sh = os.path.join(sys.path[0], "bash/do_base_recalibration.sh")
    hold_jid = prefix+'.realignment'
    out_file = os.path.join(outdir, prefix+"_recalibrated.bam")
    recal_table = os.path.join(outdir, prefix+"_recal_data.table")

    logger.debug("job_id = %s" % str(job_id))
    logger.debug("Bash script to execute: %s" % path_to_sh)
    logger.debug("Recalibration data table file '%s'" % recal_table)
    logger.info("Output bam file is '%s'" % out_file)

    command = ' '.join([path_to_sh, reference, filename, out_file, recal_table, sys.path[0]])
    cmd = create_qsub_cmd(command, job_id, hold_jid=hold_jid,path_stdout=log_dir, path_stderr=log_dir, h_vmem='20G', pe_name=settings.PE_NAME, ncores=2) if qsub else command
    runcmd(cmd)

    return out_file

def split_by_name(prefix, filename, outdir, SMs, log_dir, qsub=False):
    """
    pipeline step 1.3: split by name
    :param filename: file name to split
    :param prefix: prefix to prepend to file names
    :param SMs: SMs dict
    :param log_dir: log files directory (only applies to queue jobs)
    :param qsub: wheter or not send the job to a queue system (SGE)
    """
    logger.info("Performing split by name")
    make_dir(log_dir)
    job_id = prefix+'.splitbyname'
    path_to_sh = os.path.join(sys.path[0], "bash/do_split_by_name.sh")
    path_to_py = os.path.join(sys.path[0], "pipelineFiles/split_bam_by_name.py")
    hold_jid = prefix+'.recalibration'

    logger.debug("job_id = %s" % str(job_id))
    logger.debug("Bash script to execute: %s" % path_to_sh)
    logger.debug("Python script to execute: %s" % path_to_py)
    logger.debug("hold_jid = %s" % str(hold_jid))

    command = ' '.join([path_to_sh, path_to_py, filename, outdir, SMs, sys.path[0]])
    cmd = create_qsub_cmd(command, job_id, hold_jid=hold_jid,path_stdout=log_dir, path_stderr=log_dir, h_vmem='2G', pe_name=settings.PE_NAME, ncores=2) if qsub else command
    runcmd(cmd)


def run_preprocess_bams(tumor, normal, prefix, reference, outdir, log_dir, qsub=False):
    """
    pipeline step 1: preprocessing
    :param tumor: tumor bam file
    :param normal: normal bam file
    :param prefix: prefix to prepend to file names
    :param reference: reference fasta file
    :param outdir: out directory to save files
    :param log_dir: log files directory (only applies to queue jobs)
    :param qsub: wheter or not send the job to a queue system (SGE)
    :return: processed files
    """
    SMs = dict()
    SMs[read_sample_name(tumor)] = (tumor, "tumor")
    SMs[read_sample_name(normal)] = (normal, "normal")

    processfiles = dict()
    processfiles['tumor'] = os.path.join(outdir, os.path.basename(tumor.replace(".bam", ".recal.realing.bam")))
    processfiles['normal'] = os.path.join(outdir, os.path.basename(normal.replace(".bam", ".recal.realing.bam")))
    logger.info("Normal output bam: '%s'" % processfiles["normal"])
    logger.info("Tumor output bam: '%s'" % processfiles["tumor"])

    try:
        f = os.path.join(outdir, prefix + "_sms.json")
        logger.debug("Writing sms info to file '%s': '%s'" % (f, str(SMs)))
        with open(f, 'w') as json_sms_file:
            json.dump(SMs, json_sms_file)
    except Exception as e:
        logger.critical("Unexpected error while writing json file '%s'" % f)
        raise e

    realigned_file = realignment(prefix,tumor, normal,reference, outdir, log_dir, qsub=qsub)
    recalibrated_file = baserecalibration(prefix, realigned_file, reference, outdir, log_dir, qsub=qsub)
    split_by_name(prefix,recalibrated_file,outdir,json_sms_file.name, log_dir, qsub=qsub)

    return processfiles


def main():
    usage="""
    ************************************************************************************************************************************************************
    Task: 
    ************************************************************************************************************************************************************
    """
    parser = optparse.OptionParser(usage)
    parser.add_option("--normal", dest="normal", help="""Required. Normal Tissue bam file.""",default=None)
    parser.add_option("--tumor", dest="tumor", help="""Required. Tumor Tissue bam file.""",default=None)
    parser.add_option("--prefix", dest="prefix", help="""Required. String indicating a prefix name for output_file""", default=None)
    parser.add_option("--reference", dest="reference", help="""Required. Reference fasta file.""",default=None)
    parser.add_option("--outdir", dest="outdir", help="""Required. Full path to the directory where results will be saved.""",default=None)


    (options, args) = parser.parse_args()

    if check_parameters(options,parser):
        run_preprocess_bams(options.tumor,options.normal,options.prefix,options.reference,options.outdir)





if __name__ == '__main__':
    main()
