SUPPORTED_CALLERS = ['virmid', 'varscan2', 'somaticsniper', 'mutect']
SUPPORTED_MODE = ['full', 'calling', 'integration', 'filtration']
SUPPORTED_STATUS = ['som', 'loh', 'germ', 'all']
SUPPORTED_VARIANTS = ['snv', 'indel']
SUPPORTED_LOG_LEVEL = ["SILENT", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
PIPELINE_TRACE_FILENAME = "pipeline.trace.json"
DEFAULT_LOG_FILENAME = "pipeline.log"
PE_NAME = "smp"
