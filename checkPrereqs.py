def checkRequiredModules(mods):
    """
    check if required python modules are available to run this software
    :param mods: list of modules to check
    :return: nothing (exits on any failed test)
    """

    failedModules = list()
    for mod in mods:
        try:
            __import__(mod)
        except ImportError:
            failedModules.append(mod)

    if failedModules != list():
        raise SystemExit("Error: could not load modules '%s'. Aborting" % failedModules)

    return
