#!/usr/bin/env python

# the very first check is to test the required python modules to run properly
try:
    import checkPrereqs
except ImportError:
    raise SystemExit("Error: could not load checkPrereq module. Aborting")
# checkPrereqs.checkRequiredModules(["sys", "argparse", "logging", "os", "settings", "pysam", "numpy", "lib.utils", "pipelineFiles.pipeline"])
checkPrereqs.checkRequiredModules(["sys", "argparse", "logging", "os", "settings", "pysam", "numpy", "json", "subprocess"])

from lib.utils import make_dir
from pipelineFiles.pipeline import pipeline as pip
import sys
import argparse
import logging
import os
import settings

TEXTCHARS = ''.join(map(chr, [7, 8, 9, 10, 12, 13, 27] + range(0x20, 0x100)))
is_binary_string = lambda bytes: bool(bytes.translate(None, TEXTCHARS))

def parse_option():
    usage = """
    ************************************************************************************************************************************************************
    Task: Run a Variant Integration Analysis Pipeline
    Output: pipeline steps directory tree with output files. The file 'parameters.txt' contains the options of the executed pipeline
    ************************************************************************************************************************************************************

        Mode: full
        ---------------------
        usage: %(prog)s --mode full --normal FILE --tumor FILE --reference FILE --out DIR --prefix STRING --tmp DIR
        [--callers LIST] [--status LIST] [--workingdir DIR] [--qsub BOOLEAN] [--minPCOUNT INT] [--log-level LEVEL] [--join-variant BOOLEAN] [--log-file [FILE|+]]


        Mode: calling
        ---------------------
        usage: %(prog)s --mode calling --workingdir DIR --prefix STRING --out DIR --reference FILE
        [--callers LIST] [--status LIST] [--qsub BOOLEAN] [--minPCOUNT INT] [--log-level LEVEL] [--join-variant BOOLEAN] [--log-file [FILE|+]]


        Mode: integration
        ---------------------
        usage: %(prog)s --mode integration --workingdir DIR --prefix STRING --out DIR
        [--callers LIST] [--qsub BOOLEAN] [--status OPTIONAL] [--minPCOUNT INT] [--log-level LEVEL] [--join-variant BOOLEAN] [--log-file [FILE|+]]


        Mode: filtration
        ---------------------
        usage: %(prog)s --mode filtration --workingdir DIR --prefix STRING --out DIR
        [--qsub BOOLEAN] [--status OPTIONAL] [--minPCOUNT INT] [--log-level LEVEL] [--join-variant BOOLEAN] [--log-file [FILE|+]]


    Alternatively, you can load the config from a file:

        usage: %(prog)s --config FILE

        Config file example:
            mode = full
            out = /dir/to/out
            ...
            # this is a comment
            callers = virmid,mutect
            ...

    """

    def _validBamFile(bam):
        """
        check if "bam" is a valid bam file (extension, binary and existence)
        :param bam: filename to check
        :return: bam filename, else raise error
        """
        if not (os.path.isfile(bam) or os.path.islink(bam)):
            raise argparse.ArgumentTypeError("ERROR: bam file '%s' does not exist." % bam)

        if bam[-4:] != '.bam':
            raise argparse.ArgumentTypeError("ERROR: '%s' must have .bam extension." % bam)

        with open(bam) as f:
            if not is_binary_string(f.read(3)):
                raise argparse.ArgumentTypeError("ERROR: bam '%s' must be a binary file. Please check file format" % bam)
        return bam


    def _existentDir(d):
        """
        check if directory "d" exists
        :param d: directory to check
        :return: directory name, else raise error
        """
        if not os.path.isdir(d):
            raise argparse.ArgumentTypeError("ERROR: no such directory '%s'" % d)
        return d

    def _existentFile(f):
        """
        check if "f" file is a regular and existent file
        :param f: filename to check
        :return: file name, else raise error
        """
        if not os.path.isfile(f):
            raise argparse.ArgumentTypeError("ERROR: no such file '%s'" % f)
        return f

    def _validCallers(callers):
        """
        check if "callers" list are supported by the pipeline. Compares against SUPPORTED_CALLERS parameter in settings.py
        :param callers: space separated strings with callers names. Ex: virmid somaticsniper mutect
        :return: callers list, else raise error
        """
        if not set(callers.split()).issubset(set(settings.SUPPORTED_CALLERS)):
            raise argparse.ArgumentTypeError("ERROR: unsupported caller(s): '%s'. Actually supported callers: %s" % (" ".join(set(callers.split()) - set(settings.SUPPORTED_CALLERS)), ', '.join(settings.SUPPORTED_CALLERS)))
        return callers

    def _validBoolean(value):
        """
        check if string "value" is "true" or "false", case insensitive
        :param value: string to check
        :return: lowercase "value" string, else raise error
        """
        if not str(value).lower() in ["true", "false"]:
            raise argparse.ArgumentTypeError("ERROR: invalid booelan value '%s'" % value)
        return str(value).lower()

    def _validLogLevel(value):
        """
        check if "value" string is a supported loglevel compared against SUPPORTED_LOG_LEVEL parameter in settings.py
        :param value: string to check
        :return: lowercase "value" string, else raise error
        """
        if not str(value).lower() in map(str.lower, settings.SUPPORTED_LOG_LEVEL):
            raise argparse.ArgumentTypeError("ERROR: invalid log level value '%s'" % value)
        return str(value).lower()

    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument("--mode", dest="mode", required=False, choices=settings.SUPPORTED_MODE, help = "Pipeline run mode {full, calling, integration, filtration} to indicate the analysis step", default=None)
    parser.add_argument("--qsub", metavar="BOOLEAN", dest="qsub", required=False, type=_validBoolean, help="Send to queue (qsub) if set to True (case insensitive). Default=True.", default=True)
    parser.add_argument("--prefix", dest="prefix", required=False, help="Prefix name for output_file", default=None)
    parser.add_argument("--out", dest="out", required=False, help="Directory where results will be saved. Will be created if it does not exist", default=None)
    parser.add_argument("--normal", dest="normal", required=False, help="Normal Tissue bam file.", type=_validBamFile, default=None)
    parser.add_argument("--tumor", dest="tumor", required=False, help="Tumor Tissue bam file.", type=_validBamFile, default=None)
    parser.add_argument("--reference", dest="reference", required=False, help="fasta file containing the reference chromosomes.", type=_existentFile, default=None)
    parser.add_argument("--callers", dest="callers", nargs='+', required=False, help="Space separated list of callers to be used. Default = %s" % " ".join(settings.SUPPORTED_CALLERS), type=_validCallers, default = " ".join(settings.SUPPORTED_CALLERS))
    parser.add_argument("--status", dest="status", required=False, choices=settings.SUPPORTED_STATUS, help="{%s} set which kind of variants are considered, Default=som." % ",".join(settings.SUPPORTED_STATUS), default='som')
    parser.add_argument("--workingdir", dest="workingdir", required=False, type=_existentDir, help="Standard working directory where pipeline trace file resides", default=None)
    parser.add_argument("--minPCOUNT", dest="minPCOUNT", required=False, type=int, choices=[1, 2, 3], help="Min PCOUNT. Default=2.", default=2)
    parser.add_argument("--config", dest="config", required=False, type=_existentFile, help="Load configuration from FILE", default=None)
    parser.add_argument("--loglevel", dest="loglevel", required=False, type=_validLogLevel, help="Set log level verbosity (case insensitive): %s. Default = SILENT" % settings.SUPPORTED_LOG_LEVEL, default="SILENT")
    parser.add_argument("--logfile", metavar="LOGFILE|+", dest="logfile", required=False, help="Set log target file. Default is standard error output. If '+' is specified as a filename, logfile '%s' will be saved into 'out' directory" % settings.DEFAULT_LOG_FILENAME, default=None)
    parser.add_argument("--joinvariant", metavar="BOOLEAN", dest="joinvariant", required=False, type=_validBoolean, help="Join files with same somatic status and different variant. Default=True.", default=True)

    return parser



def _checkRequiredAndOptionalArgs(passedArgs, requiredArgs, optionalArgs, options, parser):
    if set(passedArgs) - set(requiredArgs + optionalArgs) != set():
        parser.error("Some invalid arguments for '%s' mode found: %s" % (options.mode, [f for f in set(passedArgs) - set(requiredArgs + optionalArgs)]))
    if None in [ getattr(options, f) for f in requiredArgs ]:
        parser.error("Some arguments are required in '%s' mode. Missing parameters: %s" % (options.mode, [ f[1] for f in filter(lambda x: x[0] is None, zip([ getattr(options, f) for f in requiredArgs ], requiredArgs)) ]))

    return


def check_parameters(options, parser):
    passedArgs = map(lambda x: x.split("=")[0], map(lambda x: x.lstrip("-"), filter(lambda x: x.startswith("-"), sys.argv[1:])))
    # read from config file if specified
    if not options.config is None:
        with open(options.config) as f:
            l = list()
            passedArgs = list()
            for line in f:
                if not line.startswith("#"):
                    _line = map(lambda x: x.strip("\n").strip(), line.split("="))
                    if _line[0] != "":
                        # it is not an empty line
                        l.append("--"+_line[0])
                        l.append(_line[1])
                        passedArgs.append(_line[0])
        options = parser.parse_args(l)

    if options.mode == 'full':
        requiredArgs = ["mode", "normal", "tumor", "reference", "prefix", "out"]
        optionalArgs = ["status", "callers", "minPCOUNT", "qsub", "loglevel", "logfile", "joinvariant"]

    elif options.mode == 'calling':
        requiredArgs = ["mode", "reference", "workingdir", "prefix", "out"]
        optionalArgs = ["status", "callers", "minPCOUNT", "qsub", "loglevel", "logfile", "joinvariant"]

    elif options.mode == 'integration':
        requiredArgs = ["mode", "workingdir", "prefix", "out"]
        optionalArgs = ["status", "minPCOUNT", "qsub", "callers", "loglevel", "logfile", "joinvariant"]

    elif options.mode == 'filtration':
        requiredArgs = ["mode", "workingdir", "out", "prefix"]
        optionalArgs = ["status", "minPCOUNT", "qsub", "loglevel", "logfile", "joinvariant"]

    else:
        parser.error("Unrecognized mode. Supported modes %s" % settings.SUPPORTED_MODE)

    _checkRequiredAndOptionalArgs(passedArgs, requiredArgs, optionalArgs, options, parser)

    # also save passed options to paramaters.txt
    make_dir(options.out)
    with open(os.path.join(options.out, "parameters.txt"), "w") as f:
        for op in passedArgs:
            f.write("%s = %s\n" % (op, getattr(options,op)))

    return options



def set_parameters(options):
    """
    build "option" dictionary to save all pipeline passed options. mkdir output directory too
    :param options: namespace returned by parse_args
    :return: "option" dictionary
    """
    option = {}

    option['mode'] = options.mode
    option['callers'] = options.callers.split(" ") if isinstance(options.callers, str) else options.callers[0].split(" ")

    option['out'] = options.out
    option['reference'] = options.reference
    option['prefix'] = options.prefix
    option['status'] = options.status
    option['workingdir'] = options.workingdir
    option['qsub'] = str(options.qsub).lower() == "true"
    option['minPCOUNT'] = int(options.minPCOUNT)
    option['loglevel'] = options.loglevel.upper()
    option['logfile'] = options.logfile if str(options.logfile) != "+" else os.path.join(option['out'], settings.DEFAULT_LOG_FILENAME)
    option['normal'] = options.normal
    option['tumor'] = options.tumor
    option['joinvariant'] = str(options.joinvariant).lower() == "true"

    make_dir(option['out'])

    return option

def _setup_custom_logger(name, options):
    """
    sets up the program logger, depending on logfile and loglevel options
    :param name: name of the logger
    :param options: logger options
    :return: logger instance
    """
    logger = logging.getLogger(name)

    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s', datefmt='%Y/%m/%d %H:%M:%S')

    if options['loglevel'] == "SILENT":
        handler = logging.FileHandler(os.devnull)
        logger.disabled = 1
    else:
        if options['logfile'] is None:
            handler = logging.StreamHandler(sys.stderr)
        else:
            handler = logging.FileHandler(options['logfile'])
        logger.setLevel(getattr(logging, options['loglevel']))

    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def main():
    """
    --- MAIN PROGRAM ---
    """

    parser = parse_option()
    options = parser.parse_args()
    options = check_parameters(options, parser)
    options = set_parameters(options)
    logger = _setup_custom_logger("root", options)

    pipeline = pip(options)
    if options['logfile'] is not None and os.path.isfile(options['logfile']) and os.path.getsize(options['logfile']):
        logger.info("*** Appending data to existent log file! ***")
    logger.info(" -- Starting main program -- ")
    logger.info("Pipeline mode: %s" % pipeline.options['mode'])
    logger.info("Out directory is '%s'" % str(pipeline.options['out']))
    logger.info("Working directory is '%s'" % str(pipeline.options['workingdir']))
    logger.info("Callers: %s" % str(pipeline.options['callers']))

    continuePipeline = False
    pipelineTrace = None

    # call the corresponding pipeline step
    if options['mode'] == 'full' or options['mode'] == 'preprocess':
        pipeline.run_full_mode()

    if options['mode'] == 'calling' or continuePipeline:
        pipelineTrace = pipeline.run_calling_mode(pipelineTrace)
        continuePipeline = True

    if options['mode'] == 'integration' or continuePipeline:
        pipelineTrace = pipeline.run_integration_mode(pipelineTrace if continuePipeline else None)
        continuePipeline = True
    
    if options['mode'] == 'filtration' or continuePipeline:
        pipelineTrace = pipeline.run_filtration_mode(pipelineTrace if continuePipeline else None)
        continuePipeline = True

    logger.info(" -- Finishing main program -- ")

if __name__ == '__main__':
    main()


