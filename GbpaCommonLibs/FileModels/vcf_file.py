'''
Created on 11/03/2014

@author: antonior
'''

import re
import os

from GbpaCommonLibs.DataModels import variant


class vcf_file:
    """***********************************************************************************************************************************************
    Class to model and manage vcf files
    ***********************************************************************************************************************************************"""

    def __init__(self, _filename):
        """*******************************************************************************************************************************************
        Task: to initialize object variables
        Inputs:
            _filename: string containing the name of the vcf file
        *******************************************************************************************************************************************"""
        self.filename = _filename
        self.sample = None
        self.status = None
        self.header = None

        if float(self.version) < 4:
            self._get_header_v3()
        else:
            self._get_header_v4
        self._index = None


    def _get_version(self):
        'version of the vcf'
        fhand = open(self.filename)
        first_line = fhand.readline()
        fhand.close()
        first_line = first_line.strip()
        first_line = first_line.lstrip('##')
        kind, value = first_line.split('=', 1)
        if kind not in ('fileformat', 'format'):
            msg = 'Vcf file malformed. first line must be version info'
            raise ValueError(msg)
        return value.split('v')[1]

    version = property(_get_version)

    def _get_prefix(self):
        'prefix of the vcf if it exits'

        self.prefix = {}
        for line in open(self.filename):
            if not line.startswith('#'):
                break
            if line.startswith('##'):
                line = line.strip()
                line = line.lstrip('##')
                kind, value = line.split('=', 1)
                if kind == 'prefix':
                    for pair in value.split(','):
                        prefix, caller = pair.split('=')
                        self.prefix[prefix] = caller
                    break
        return


    @property
    def _get_header_v4(self):
        fieldOrder = []
        if self.header is not None:
            return self.header
        headers = {}
        for line in open(self.filename):
            if not line.startswith('#'):
                break
            if line.startswith('##'):
                line = line.strip()
                line = line.lstrip('##')
                kind, value = line.split('=', 1)
                if kind == 'somatic_status':
                    if kind not in headers:
                        fieldOrder.append('somatic_status')
                    headers[kind] = value
                elif kind == 'FILTER':
                    if kind not in headers:
                        fieldOrder.append('FILTER')
                        headers[kind] = {}
                    values = self._parse_header_line_v4(value)
                    # headers[kind][values['ID']] = values['Description']
                    name = values['ID']
                    del values['ID']
                    headers[kind][name] = values
                elif kind in ('FORMAT', 'INFO'):
                    if kind not in headers:
                        headers[kind] = {}
                        if kind in 'FORMAT':
                            fieldOrder.append('FORMAT')
                        elif kind in 'INFO':
                            fieldOrder.append('INFO')

                    values = self._parse_header_line_v4(value)
                    name = values['ID']
                    del values['ID']
                    headers[kind][name] = values
                elif kind == 'contig':
                    if kind not in headers:
                        headers[kind] = {}
                        fieldOrder.append('contig')
                        index_element = 0
                    headers[kind][index_element] = value
                    index_element += 1
                elif kind == 'fileformat':
                    if kind not in headers:
                        fieldOrder.append('fileformat')
                    headers[kind] = value

                elif kind == 'GATKCommandLine':
                    if kind not in headers:
                        fieldOrder.append('GATKCommandLine')
                        headers[kind] = {}
                        index_element = 0

                    headers[kind][index_element] = value
                    index_element += 1
                elif kind == 'reference':
                    if kind not in headers:
                        fieldOrder.append('reference')
                    headers[kind] = value

                else:
                    headers[kind] = value
            else:
                line = line.lstrip('#')
                headers['colnames'] = line.split()
                fieldOrder.append('colnames')

        headers['fieldsOrder'] = fieldOrder  # 'fieldsOrder' has the order of each field to be printed in the VCF file
        self.header = headers

    @staticmethod
    def _parse_header_line_v4(line):
        'it parses a header line, but only the value part. between <>'
        values = {}

        line = line.replace('<ID:', '<ID=')  # To adapt virmid output

        line = line.strip('<')
        line = line.strip('>')

        # To avoid multiple , and = in descritpion field we separate it
        line, description = re.split(',\s*Description\s*=', line)
        values['Description'] = description.strip('"')

        for pair in line.split(','):
            key, value = pair.split('=')
            values[key] = value.strip('"')
        return values

    def _get_vcfs(self):
        'vcf generator'
        colnames = self.header['colnames']
        for line in open(self.filename):
            if line.startswith('#'):
                continue
            yield self._parse_vcf_line(line, colnames)

    vcfs = property(_get_vcfs)

    def _parse_vcf_line(self, line, colnames):
        '''It parses the cvf svn line'''
        vcf_items = line.split()
        vcf = dict(zip(colnames[:9], vcf_items[:9]))

        # reformat FILTER
        vcf['FILTER'] = vcf['FILTER'].split(';')
        # REformat INFO
        info = vcf['INFO']
        vcf['INFO'] = {}
        for info_ in info.split(';'):
            if len(info_.split('=')) != 1:  # # If INFO is empty do not do anything
                info_key, info_value = info_.split('=')
                vcf['INFO'][info_key] = info_value

        # reformat FORMAT if this field exists
        if vcf.has_key('FORMAT'):
            vcf['FORMAT'] = vcf['FORMAT'].split(':')  # this was a dict previously. Check conflicts ##TODO

            vcf['samples'] = {}
            for samples in zip(colnames[9:], vcf_items[9:]):
                vcf['samples'][samples[0]] = samples[1].split(':')
        else:
            vcf['FORMAT'] = []
            vcf['samples'] = {}

        return vcf

    def _make_index(self):
        '''it makes an index of the vcf file. It takes the vcf position
        (chrom, position) as index'''
        if self._index is not None:
            return self._index
        index = {}
        fhand = open(self.filename, 'rt')
        rawline = 'filled'
        while len(rawline) != 0:
            prior_tell = fhand.tell()
            rawline = fhand.readline()
            if rawline and rawline[0] == '#':
                continue
            index[tuple(rawline.split()[:2])] = prior_tell
        index.pop(())
        self._index = index


    def get_snv(self, position):
        'It returns an snv giving it position'
        colnames = self.header['colnames']
        if self._index is None:
            self._make_index()
        fhand = open(self.filename)
        file_position = self._index[position]
        fhand.seek(file_position)
        return self._convertLineToVariant(fhand.readline())


    def _get_header_lines(self):
        'It gets an iterator with header lines and colnames'
        for line in open(self.filename):
            if line.startswith('#'):
                yield line
            else:
                break

    def _get_snv_line(self, position):
        'It returns an snv LINE giving it position'
        if self._index is None:
            self._make_index()
        fhand = open(self.filename)
        file_position = self._index[position]
        fhand.seek(file_position)
        return fhand.readline()

    def nvariants(self):
        '''**********************************
        Task: Count variants (mutated sites)
        **********************************'''

        fd = file(self.filename)
        self.skip_header(fd)
        header = fd.readline()
        variants = 0
        for line in fd:
            parts = line.split('\t')

            if (parts[4] <> '.'):
                variants += 1

        fd.close()

        return variants

    def load_sample(self):
        '''**********************************
        Task: initalize samples names
        **********************************'''

        fd = file(self.filename)
        self.skip_header(fd)
        self.sample = fd.readline().replace("\n", "").split("\t")[9:]
        print self.sample
        fd.close()

    @staticmethod
    def skip_header(fd):
        """*******************************************************************************************************************************************
        :rtype : file
        Task: skips all those lines in the vcf starting with ##
        Inputs:
            fd: file descriptor.
        Outputs:
            fd: the file descriptor pointing to the first character of the actual table (>>>to the header of the table<<<)
        *******************************************************************************************************************************************"""

        # Lines starting with ## are skipped. The file descriptor is left pointing to the first character of the first line that does not start with ##
        line = fd.readline()
        while (line.startswith('##')): line = fd.readline()
        fd.seek(-len(line), 1)

        return fd

    @staticmethod
    def skip_and_write_header(fdin, fdout):
        line = fdin.readline()
        while (line.startswith('##')):
            fdout.write(line)
            line = fdin.readline()
        fdin.seek(-len(line), 1)

        return fdin, fdout


    def _convertLineToVariant(self, line, type_f='Normal', KG=False):
        fd = file(self.filename)
        fd = self.skip_header(fd)

        header = fd.readline().replace("\n", "").split("\t")
        fd.close()

        i = 9
        a_line = line.replace('"', "").replace(";;", ";").replace("\n", "").split("\t")

        genoInfo = {}
        try:
            qual = float(a_line[5])
        except:
            qual = a_line[5]

        if KG:
            a_line.pop()

        if "FORMAT" in header:
            format = a_line[8].split(":")

        if len(a_line) > 9:
            samples = []
            while i <= len(a_line) - 1:
                # Parser genotypeinfo per sample
                genoInfo_hash = {}
                for ind, format_filed in enumerate(format):
                    genoInfo_hash[format_filed] = a_line[i].split(":")[ind]
                genoInfo[header[i]] = genoInfo_hash

                #Parser Samples
                samples.append(header[i])
                i = i + 1

            if type_f == 'Normal':
                aux_variant = variant.variant(a_line[0], int(a_line[1]), a_line[2], a_line[3], a_line[4], qual,
                                              a_line[6], a_line[7], samples, genoInfo)
            if type_f == 'loh':
                aux_variant = variant.loh_variant(a_line[0], int(a_line[1]), a_line[2], line, a_line[3], a_line[4],
                                                  qual, a_line[6], "info", samples, genoInfo)
        else:
            if type_f == 'Normal':
                aux_variant = variant.variant(a_line[0], int(a_line[1]), a_line[2], a_line[3], a_line[4], qual,
                                              a_line[6], a_line[7])

        return aux_variant

    def get_variants(self, type_f='Normal', KG=False):
        '''**********************************
        Task: Return a list of all variants (warning with multi-allelic variants!!!)
        Inputs:
            type_f: type of varaint will be return: Normal (default) o loh (for loss of heterocigosity variants)
        **********************************'''
        fd = file(self.filename)
        fd = self.skip_header(fd)

        header = fd.readline().replace("\n", "").split("\t")

        for line in fd:
            i = 9
            a_line = line.replace('"', "").replace(";;", ";").replace("\n", "").split("\t")

            genoInfo = {}
            try:
                qual = float(a_line[5])
            except:
                qual = a_line[5]

            if KG:
                a_line.pop()

            if "FORMAT" in header:
                format = a_line[8].split(":")

            if len(a_line) > 9:
                samples = []
                while i <= len(a_line) - 1:
                    # Parser genotypeinfo per sample
                    genoInfo_hash = {}
                    for ind, format_filed in enumerate(format):
                        if ind < len(a_line[i].split(
                                ":")):  # May be a variant is '.' or .,.,. in INFO fields. We have to take this into account
                            genoInfo_hash[format_filed] = a_line[i].split(":")[ind]
                        else:
                            genoInfo_hash[format_filed] = '.'

                    genoInfo[header[i]] = genoInfo_hash

                    #Parser Samples
                    samples.append(header[i])
                    i = i + 1

                if type_f == 'Normal':
                    aux_variant = variant.variant(a_line[0], int(a_line[1]), a_line[2], a_line[3], a_line[4], qual,
                                                  a_line[6], a_line[7], samples, genoInfo)
                if type_f == 'loh':
                    aux_variant = variant.loh_variant(a_line[0], int(a_line[1]), a_line[2], line, a_line[3], a_line[4],
                                                      qual, a_line[6], "info", samples, genoInfo)
            else:
                if type_f == 'Normal':
                    aux_variant = variant.variant(a_line[0], int(a_line[1]), a_line[2], a_line[3], a_line[4], qual,
                                                  a_line[6], a_line[7])

            yield aux_variant


    def concat_files(self, other, outfile):
        fd1 = file(self.filename)
        fd2 = file(other.filename)
        fdw = file(outfile, 'w')
        fd1, fdw = self.skip_and_write_header(fd1, fdw)
        fd2 = self.skip_header(fd2)

        header1 = fd1.readline()
        header2 = fd2.readline()

        if header1 != header2:
            print header1 + " " + header2
            return False

        else:
            fdw.write(header1)
            for line in fd1:
                fdw.write(line)
            for line in fd2:
                fdw.write(line)

        print outfile
        vcf_out = vcf_file(outfile)
        fdw.close()
        fd1.close()
        fd2.close()

        return vcf_out

    @staticmethod
    def concatSortedVcfs(vcfs, out):
        """
        Concatenate a list of vcfs. All vcf files *MUST* be previously sorted
        :param vcfs: list of vcf filenames (not vcf_file objects!) to concat
        :param out: sorted and concatenated vcf out file
        """

        vcfsObjectList = [ vcf_file(f) for f in vcfs ]

        # if vcfs have not the same colnames or fileformat in header, cannot continue
        if reduce(lambda x, y: x if x == y else None, [header['colnames'] for header in [vcf.header for vcf in vcfsObjectList]]) is None\
        or reduce(lambda x, y: x if x == y else None, [header['fileformat'] for header in [vcf.header for vcf in vcfsObjectList]]) is None:
                return False

        # write merged metainfo and colnames to out file
        with open(out, 'w') as outfd:
            outfd.write("##fileformat=%s\n" % vcfsObjectList[0].header['fileformat'])
            for vcfObject in vcfsObjectList:
                with open(vcfObject.filename) as f:
                    outfd.write("##header_source=%s\n" % vcfObject.filename)
                    for line in f:
                        if line.startswith("##"):
                            if not line.startswith("##fileformat="):
                                outfd.write(line)
                        else:
                            break
            outfd.write("#%s\n" % "\t".join(vcfsObjectList[0].header['colnames']))

        # read data (already sorted!) and write to output file
        fdsList = [open(f) for f in vcfs]
        maxVariant = variant.variant("ZZZZZZZZ", 9999999)
        variantGeneratorsList = [f.get_variants() for f in vcfsObjectList]
        variantList = [next(f, maxVariant) for f in variantGeneratorsList]
        with open(out, 'a') as outfd:
            while filter(lambda x: x != maxVariant, variantList) != []:
                m = min(variantList)
                variantList[variantList.index(m)] = next(variantGeneratorsList[variantList.index(m)], maxVariant)
                outfd.write("%s\n" % m)
        for fd in fdsList:
            fd.close()

        return True

    def sort_vcftools(self, pathvcftools, outfile, c=False):
        if c:
            os.system(pathvcftools + "/vcf-sort -c " + self.filename + " > " + outfile)
        else:
            os.system(pathvcftools + "/vcf-sort " + self.filename + " > " + outfile)

        vcf_out = vcf_file(outfile)
        return vcf_out

    def compound_heterocigosity(self, geneField, isKG=False, phase=False):

        all_variants = [v for v in self.get_variants(KG=isKG)]
        genes_variants = {}
        selected_genes = []
        selected_variants = []

        for v in all_variants:
            gene = v.info[geneField]
            mut_geno = v.get_mutated_samples(phase)

            if gene in genes_variants:
                for sample in mut_geno:
                    if phase:
                        if sample in genes_variants[gene]:
                            if genes_variants[gene][sample] != mut_geno[sample] or mut_geno[sample] == "1|1":
                                selected_genes.append(gene)
                    else:
                        if sample in genes_variants[gene] or mut_geno[sample] == "1|1":
                            selected_genes.append(gene)
            else:
                genes_variants[gene] = mut_geno

        for v in all_variants:
            if v.info[geneField] in selected_genes:
                selected_variants.append(v)

        return selected_variants


    def stats_by_sample(self, out_file, type_f='Normal', KG=False):


        total = 0
        self.load_sample()
        stats = {}
        for sample in self.sample:
            stats[sample] = {}
            stats[sample]["reference"] = 0
            stats[sample]["missing"] = 0
            stats[sample]["heterozygous"] = 0
            stats[sample]["homozygous"] = 0
            stats[sample]["variant"] = 0
            stats[sample]["transition"] = 0
            stats[sample]["transversion"] = 0
            stats[sample]["indel"] = 0
            stats[sample]["snv"] = 0

        for var in self.get_variants(type_f, KG):
            total += 1
            alts = var._alts
            ti_tv = {}
            indel_snv = {}

            for i, alt in enumerate(alts):
                if (var.ref == "A" and alt == "G") or (var.ref == "G" and alt == "A") or (
                                var.ref == "C" and alt == "T") or (var.ref == "T" and alt == "C"):
                    ti_tv[i + 1] = "transition"
                elif len(var.ref) == 1 and len(alt) == 1:
                    ti_tv[i + 1] = "transversion"

                if len(var.ref) != 1 or len(alt) != 1:
                    indel_snv[i + 1] = "indel"
                else:
                    indel_snv[i + 1] = "snv"

            for sample in self.sample:
                allele = var.genoInfo[sample]["GT"].split(":")[0].split("/")

                if allele[0] == "0" and allele[1] == "0":
                    stats[sample]["reference"] += 1


                elif allele[0] == "." and allele[1] == ".":
                    stats[sample]["missing"] += 1


                else:
                    if allele[0] != allele[1]:
                        stats[sample]["heterozygous"] += 1


                    else:
                        stats[sample]["homozygous"] += 1

                    for key in ti_tv:
                        if str(key) in allele:
                            stats[sample][ti_tv[key]] += 1

                    for key in indel_snv:
                        if str(key) in allele:
                            stats[sample]["variant"] += 1

                            stats[sample][indel_snv[key]] += 1

        fdw = file(out_file, "w")

        header = ["Sample", "Homozygous_reference_possitions", "Homozygous_alternative_possitions",
                  "Heterozygous_possitions", "Missing_possitions", "Missing_ratio", "Variants", "SNVs", "Indels",
                  "Transition_variants", "Transversion_variants", "Ratio_transition/transversion"]
        fdw.write("\t".join(header) + "\n")
        for sample in stats:
            line = [sample, stats[sample]["reference"], stats[sample]["homozygous"], stats[sample]["heterozygous"],
                    stats[sample]["missing"], float(stats[sample]["missing"]) / total, stats[sample]["variant"],
                    stats[sample]["snv"], stats[sample]["indel"], stats[sample]["transition"],
                    stats[sample]["transversion"], float(stats[sample]["transition"]) / stats[sample]["transversion"]]
            fdw.write("\t".join(map(str, line)) + "\n")
        fdw.close()


    def add_vcf_field(self, field, id, description_message):
        '''**********************************
        Task: Add a new field to VCF headerWrite header from an object to an output file
        Inputs:
            field: meta-field to be added (FILTER, FORMAT, INFO)
            id: field identified (ID)
            description_message: message corresponding to "Description"

        **********************************'''

        self.header[field][id] = {}
        self.header[field][id]['Description'] = description_message


    def write_vcf_header(self, fdw):
        '''**********************************
        Task: Write header from an object to an output file
        Inputs:
            fdw: output file fid
        **********************************'''

        for current_field in self.header['fieldsOrder']:  # The order of fields in VCF file
            if current_field == 'fileformat':
                fdw.write('##fileformat=' + self.header[current_field] + "\n")
            elif current_field == 'FILTER':
                for current_filter in self.header[current_field]:
                    fdw.write('##FILTER=<ID=' + current_filter + ',Description="' +
                              self.header[current_field][current_filter]['Description'] + '">\n')
            elif current_field == 'FORMAT':
                for current_format in self.header[current_field]:
                    fdw.write(
                        '##FORMAT=<ID=' + current_format + ',Number=' + self.header[current_field][current_format][
                            'Number'] + ',Type=' + self.header[current_field][current_format][
                            'Type'] + ',Description="' + self.header[current_field][current_format][
                            'Description'] + '">\n')
            elif current_field == 'GATKCommandLine':
                for current_command in self.header[current_field]:
                    fdw.write('##GATKCommandLine=' + self.header[current_field][current_command] + '\n')
            elif current_field == 'INFO':
                for current_info in self.header[current_field]:
                    fdw.write('##INFO=<ID=' + current_info + ',Number=' + self.header[current_field][current_info][
                        'Number'] + ',Type=' + self.header[current_field][current_info]['Type'] + ',Description="' +
                              self.header[current_field][current_info]['Description'] + '">\n')
            elif current_field == 'contig':
                for current_contig in self.header[current_field]:
                    fdw.write('##contig=' + self.header[current_field][current_contig] + '\n')
            elif current_field == 'reference':
                fdw.write('##reference=' + self.header[current_field] + '\n')
            elif current_field == 'colnames':
                fdw.write('#' + "\t".join(self.header[current_field]) + '\n')

