import json
import numpy

class variant:
    '''
    Task: Manage variants
    '''

    def __init__(self, chrom, pos, id='.', ref='.', alt='.', qual='.', filter_l='PASS', info='.', samplenames=None,
                 genoInfo=None):


        """
        :param chrom: crhomosome
        :param pos: position 1-based
        :param id: id
        :param ref: reference base
        :param alt: alternate base
        :param qual: Quality of calling
        :param filter_l: filters
        :param info: Info Fields
        :param samplenames: Array of samples
        :param genoInfo: Genotype Info
        """
        self.chrom = chrom
        self.pos = pos
        self.ref = ref
        self.alt = alt
        self.filter = filter_l
        self.qual = qual
        self.genoInfo = genoInfo
        self.missing = 0
        self.reference = 0
        self.AC = 0
        self.AF = 0
        self.alleleCount = 0
        self.NS = 0
        self.id = id
        self.sampleNames = samplenames
        self.info = {}
        self._alts = self.alt.split(",")
        self.alleles_info = {}
        for field in info.split(";"):
            if field != '.':
                try:
                    self.info[field.split("=")[0]] = field.split("=")[1]
                except:
                    self.info[field.split("=")[0]] = ""

        # mik hot fix. If variant is in standard format, "GT" does not exist in samples (but vsGT, ssGT, mtGT, etc.)
        if self.genoInfo != None:
            unsort_format = genoInfo[self.sampleNames[0]].keys()
            if "GT" in unsort_format:
                gt = unsort_format.pop(unsort_format.index("GT"))
                unsort_format.insert(0, gt)
            self.format = unsort_format
        else:
            self.format = None



    def __eq__(self, other):
        return other and self.chrom == other.chrom and self.pos == other.pos

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        if self.chrom != other.chrom:
            try:
                int(self.chrom)
                selfIsInt = True
            except ValueError:
                selfIsInt = False

            try:
                int(other.chrom)
                otherIsInt = True
            except ValueError:
                otherIsInt = False

            if selfIsInt and otherIsInt: return int(self.chrom) < int(other.chrom)

            if selfIsInt and not otherIsInt: return True
            if otherIsInt and not selfIsInt: return False
            if self.chrom == "X": return True
            if other.chrom == "X": return False
            if self.chrom == "Y": return True
            if other.chrom == "Y": return False
            return self.chrom < other.chrom
        else:
            return int(self.pos) < int(other.pos)

    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        return not self < other and not self == other

    def __ge__(self, other):
        return self > other or self == other


    def __hash__(self):
        return hash((self.chrom, self.pos, self.ref, self.alt))



    def __str__(self):
        info_values = []
        sample_genoinfo = []

        if len(self.info.keys()) > 0:
            for info_key in self.info:
                info_values.append(info_key + '=' + str(self.info[info_key]))
        else:
            info_values.append(".")

        out = [self.chrom, self.pos, self.id, self.ref, self.alt, self.qual, self.filter]
        info = ";".join(info_values)
        out.append(info)

        if self.genoInfo:
            for sample in self.sampleNames:
                tags = []
                for format_filed in self.format:
                    if format_filed in self.genoInfo[sample]:
                        tags.append(self.genoInfo[sample][format_filed])
                    else:
                        tags.append(".")
                sample_genoinfo.append(":".join(tags))

            out.append(":".join(self.format))
            out = out + sample_genoinfo

        return "\t".join(map(str, out))


    def __sub__(self, other):
        if self.chrom == other.chrom:
            return self.pos - other.pos
        else:
            raise Exception("Dfferent chromosomes")



    def check_filter(self, filter_value='PASS'):
        """
        :param filter_value: string to filter
        :return: boolean
        """
        if self.filter == filter_value:
            return True
        else:
            return False



    def variant_to_json(self, info_list=None):

        json_list = []
        for i, alt in enumerate(self._alts):


            varaint_object = {"_id": self.chrom + "_" + str(self.pos) + "_" + self.ref + "_" + alt,
                              "chrom": self.chrom,
                              "pos": self.pos,
                              "alts": alt,
                              "ref": self.ref
            }

            if self.genoInfo != None:
                varaint_object["samples"] = self.genoInfo

            if info_list != None:
                for key in self.info:
                    # TODO Fix multivariant
                    if key in info_list:
                        try:
                            varaint_object[key] = int(self.info[key].split(",")[i])
                        except:
                            try:
                                varaint_object[key] = float(self.info[key].split(",")[i])
                            except:
                                varaint_object[key] = self.info[key].split(",")[i]

            json_list.append(json.JSONEncoder().encode(varaint_object))
        return "\n".join(json_list)

    def annotation_format(self, info_list):
        info_values = []
        info_header = []

        for info_key in info_list:
            if info_key in self.info:
                info_header.append(info_key)
                info_values.append(self.info[info_key])
            else:
                info_header.append(info_key)
                info_values.append(".")

        header = ['chr', 'pos', 'ref', 'alt']
        header = header + info_header

        out = [self.chrom, self.pos, self.ref, self.alt]
        out = out + info_values

        return out


    def annotate_frecuency(self):
        # initialize all variants in order to save values
        for alt in self._alts:
            self.alleles_info[alt] = {"AC": 0,
                                      "AF": 0,
                                      "SN": 0
            }

        if self.genoInfo:
            for sample in self.genoInfo:

                diff_allele = {}

                if "/" in self.genoInfo[sample]["GT"]:
                    alleles_index = self.genoInfo[sample]["GT"].split("/")

                elif "|" in self.genoInfo[sample]["GT"]:
                    alleles_index = self.genoInfo[sample]["GT"].split("|")

                else:
                    alleles_index = self.genoInfo[sample]["GT"]

                for index in alleles_index:

                    if index == ".":
                        self.missing += 1
                    elif index == "0":
                        self.reference += 1
                        self.alleleCount += 1
                    else:
                        alt = self._alts[int(index) - 1]
                        diff_allele[alt] = 1
                        self.alleles_info[alt]["AC"] += 1
                        self.alleleCount += 1

                for key in diff_allele:
                    self.alleles_info[key]["SN"] += 1

        for alt in self.alleles_info:
            try:
                self.alleles_info[alt]["AF"] = (self.alleles_info[alt]["AC"] * 1.0) / self.alleleCount
            except ZeroDivisionError:
                self.alleles_info[alt]["AF"] = "nan"

        AF_info = []
        AC_info = []
        sample_number = []
        for alt in self._alts:
            AF_info.append(self.alleles_info[alt]["AF"])
            AC_info.append(self.alleles_info[alt]["AC"])
            sample_number.append(self.alleles_info[alt]["SN"])

        self.AF = AF_info
        self.info["AF"] = ",".join(map(str, AF_info))
        self.info["SN"] = ",".join(map(str, sample_number))
        self.info["AC"] = ",".join(map(str, AC_info))
        self.info["NS"] = self.missing


    def drop_info(self, keep=[None]):
        infotodelete = []
        for key in self.info:
            if key not in keep:
                infotodelete.append(key)
        for delete in infotodelete:
            self.info.pop(delete)


    def select_by_info_from_list(self, _listvalue, field):

        fd = file(_listvalue)
        list_go = []
        for line in fd:
            list_go.append(line.replace("\n", ""))
        for value in self.info[field].split(","):
            if value in list_go:
                return True
            else:
                return False


    def select_by_info_numeric(self, method, value, field):

        value = float(value)

        variants_values = numpy.array(map(float, self.info[field].split(",")))

        max_value = variants_values.max()
        min_value = variants_values.min()

        if method == "greater":
            if min_value > value:
                return True
            else:
                False

        if method == "lower":
            if max_value < value:
                return True
            else:
                False

    def get_mutated_samples(self, phase=False):
        mutated_geno = {}
        for sample in self.genoInfo:
            if phase:
                if self.genoInfo["GT"] != "0|0" or self.genoInfo["GT"] != ".|." or self.genoInfo["GT"] != "0":
                    mutated_geno[sample] = self.genoInfo["GT"]
            else:
                if self.genoInfo["GT"] != "0/0" or self.genoInfo["GT"] != "./." or self.genoInfo["GT"] != "0":
                    mutated_geno[sample] = self.genoInfo["GT"]
        return mutated_geno

    def select_variant_by_genotypes(self, genotypes_required):
        current_genotypes = []
        for sample in genotypes_required:
            try:
                if (self.genoInfo[sample]["GT"] != "." and self.genoInfo[sample]["GT"] != "./." and self.genoInfo[sample]["GT"] != ".|.") and self.genoInfo[sample]["GT"] != genotypes_required[sample]:
                    print "Variant was filtered out due to: genotype for sample " + sample + " is " + \
                          self.genoInfo[sample]["GT"] + " and " + genotypes_required[sample] + " is required"
                    return False
                current_genotypes.append(self.genoInfo[sample]["GT"])
            except KeyError:
                print sample + " is not in this variant"
                return False

        if len(set(current_genotypes).difference(set(['0', '.', '0/0', '0|0', '.|.', './.']))) > 0: # If any of the genotypes is not ['0', '.', '0/0', '0|0', '.|.', './.']
            return True
        else:
            return False



    def annot(self, keys, values):
        """
        Task: Add annotation

        :param keys:
        :param values:
        """
        for i, key in enumerate(keys):
            if isinstance(values[i], list):
                self.info[key] = ",".join(values[i])
            else:
                self.info[key] = values[i]





    def custom_filter_type_1(self, percentage, not_pass_message):
        '''**********************************
        Task: Filter applied to variants. A variant is filtered if the allele with the maximum frequency is present in less than percentage of reads
        Variants not passing this filter are tagged as 'NOT_PASS_CUSTOM_1'
        **********************************'''

        if self.genoInfo and 'AD' in self.format and 'DP' in self.format:
            for sample in self.sampleNames:
                ad_values = self.genoInfo[sample]['AD']
                dp_value = self.genoInfo[sample]['DP']


                if ad_values != '.' and dp_value != '.':


                    # get maximum allele value
                    ad_max = max(map(int, ad_values.split(',')))
                    current_freq = ad_max / float(dp_value)
                    if not 'FT' in self.format:  # Filter tag is in genotype
                        self.format.append('FT')

                    if (current_freq < percentage):  # This variant does not pass
                        if 'FT' in self.genoInfo[sample].keys():
                            if self.genoInfo[sample]['FT'].strip() == 'PASS':
                                self.genoInfo[sample]['FT'] = not_pass_message
                            else:  # There are other filters NOT PASS
                                self.genoInfo[sample]['FT'] = self.genoInfo[sample]['FT'] + ',' + not_pass_message
                        else:
                            self.genoInfo[sample]['FT'] = not_pass_message
                    else:
                        if not 'FT' in self.genoInfo[sample].keys():
                            self.genoInfo[sample]['FT'] = 'PASS'

                            # If FT is in this sample, since current_freq is above percentage, we keep it as it is
                else:
                    if 'FT' in self.genoInfo[sample].keys():
                        if self.genoInfo[sample]['FT'].strip() == 'PASS':
                            self.genoInfo[sample]['FT'] = not_pass_message
                        else:  # There are other filters NOT PASS
                            self.genoInfo[sample]['FT'] = self.genoInfo[sample]['FT'] + ',' + not_pass_message
                    else:
                        self.genoInfo[sample]['FT'] = not_pass_message
        else:
            print 'Allele Depth (AD) and Depth (DP) not found in genotypes. Exiting'
            exit()

    # mik added
    def normalize(self, prefix="_"):

        '''
        add prefix to the INFO, FORMAT, FILTER and GENOINFO fields
        add 'qual' field to 'info' field if applicable ('qual'!='.'), with prefix
        normalizes info values. If key X is found, normalizes to X=1 (DB mutect key for example)

        1 - if SS does not exist in INFO, get SS from TUMOR sample and add to INFO field (if exists)
        2 - add prefix to the INFO, FORMAT, FILTER and GENOINFO fields. Replace '.' by 'PASS' in FILTER field
        3 - normalizes info values. If key X is found, normalizes to X=1 (DB mutect key for example)
        4 - add 'qual' field to 'info' field if applicable ('qual'!='.'), with prefix
        '''

        # 1 - if SS does not exist in INFO, get SS from TUMOR sample and add to INFO field (if exists)
        try:
            if not self.info.has_key("SS"): self.info["SS"] = self.genoInfo["TUMOR"]["SS"]
        except KeyError:
            pass

        # 2 - add prefix to the INFO, FORMAT, FILTER and GENOINFO fields
        # and step 3
        for info_key in self.info.keys():
            new_info_key = prefix + info_key
            # Add the new key and remove the old one
            self.info[new_info_key] = self.info.pop(info_key)
            self.info[new_info_key] = self.info[new_info_key] + "1" if self.info[new_info_key] == '' else self.info[
                new_info_key]
        # add prefix to genoinfo keys:
        try:
            for item in self.genoInfo.items():
                for key in item[1].keys():
                    item[1][prefix + key] = item[1].pop(key)
        except AttributeError:
            pass
        # add prefix to format names:
        try:
            self.format = [prefix + format_key for format_key in self.format]
        except TypeError:
            pass
        # add prefix to filter names and replace '.' by 'PASS'
        self.filter = ";".join([prefix + f if f != '.' else prefix + 'PASS' for f in self.filter.split(";")])


        # 4 - add 'qual' field to 'info' field if applicable ('qual'!='.'), with prefix
        if self.qual != '.':
            new_info_key = prefix + 'QUAL'
            self.info[new_info_key] = self.qual

    # mik added
    def somatizeMe(self):
        ''' tries to convert a variant into a somatized version (self)
        returns True if successful or False otherwise
        '''

        try:
            GTs_normal = list()
            GTs_tumor = list()
            PCOUNT1 = PCOUNT2 = PCOUNT3 = 0
            PNAME1 = PNAME2 = PNAME3 = ""
            for filter in self.filter.split(";"):
                prefix = filter[:2]
                action = filter[2:]
                if action == "PASS":
                    # it is a xxPASS filter

                    # save genotypes. both NORMAL and TUMOR; for a later consensus
                    GTs_normal.append(self.genoInfo['NORMAL'][prefix + "GT"])
                    GTs_tumor.append(self.genoInfo['TUMOR'][prefix + "GT"])

                    # set PCOUNT and PNAME
                    if self.info[prefix + "SS"] == "1":
                        PCOUNT1 += 1
                        PNAME1 = ",".join([PNAME1] + [prefix]).lstrip(",")
                    elif self.info[prefix + "SS"] == "2":
                        PCOUNT2 += 1
                        PNAME2 = ",".join([PNAME2] + [prefix]).lstrip(",")
                    elif self.info[prefix + "SS"] == "3":
                        PCOUNT3 += 1
                        PNAME3 = ",".join([PNAME3] + [prefix]).lstrip(",")


            # ## update variant object with new info: GT, PCOUNT and PNAME
            # if all filters were set to REJECT, GT=./.
            self.format.insert(0, "GT")
            if GTs_normal == [] and GTs_tumor == []:
                self.genoInfo['NORMAL']['GT'] = "./."
                self.genoInfo['TUMOR']['GT'] = "./."

            # otherwise, the majority
            else:
                self.genoInfo['NORMAL']['GT'] = max(set(GTs_normal), key=GTs_normal.count)
                self.genoInfo['TUMOR']['GT'] = max(set(GTs_tumor), key=GTs_tumor.count)

            # save PCOUNT and PNAME in 'info' field
            self.info['PCOUNT1'] = str(PCOUNT1)
            self.info['PCOUNT2'] = str(PCOUNT2)
            self.info['PCOUNT3'] = str(PCOUNT3)
            self.info['PNAME1'] = PNAME1 if PNAME1 != "" else "NONE"
            self.info['PNAME2'] = PNAME2 if PNAME2 != "" else "NONE"
            self.info['PNAME3'] = PNAME3 if PNAME3 != "" else "NONE"

            return True

        except Exception as e:
            raise Exception(e)

    def modify_genotype(self):
        """
        This method examines genotype filters for each sample. If NOT_PASS is found, set corresponding genotype to .
        """

        if 'FT' in self.format:
            for sample in self.sampleNames:
                if 'NOT_PASS' in self.genoInfo[sample]['FT']:
                    self.genoInfo[sample]['GT'] = '.'

        else:
            print 'Filter (FT) not found in genotypes. Exiting'
            exit()


class loh_variant(variant):

    def check_cluster(self, other, limit=1000):
        '''
        check if two loh_varaitns are in the same cluster
        '''

        if self.chrom == other.chrom:
            if abs(self - other) <= limit:
                return True
            else:
                return False
        else:
            return False

