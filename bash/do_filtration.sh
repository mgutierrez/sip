#!/bin/bash

rootPath=$6
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

# Variables
vcf=$1
outfile=$2
ss=$3
min=$4
script=$5

# perform some sanity checks
filesToCheck="script"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done


# Run
python ${script} --vcf ${vcf} --outfile ${outfile} --minPCOUNT ${min} --ss ${ss}
