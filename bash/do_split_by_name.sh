#!/bin/bash

rootPath=$5
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

# options
script=$1
filename=$2
outdir=$3
SMs=$4

# perform some sanity checks
filesToCheck="script filename"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done

python ${script} --filename ${filename} --outdir ${outdir} --sms_file ${SMs}

# Create index with samtools
for file in ${outdir}/*recal.realing.bam; do samtools index ${file}; done
