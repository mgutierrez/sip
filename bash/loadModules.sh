# this file is sourced by the different bash scripts contained in this directory
# it is intended to make all the necessary python modules and other executable files available to those bash scripts
# it's up to you to make them available by, for example, modifying PYTHONPATH or using the environment modules software

# this var contains the path where SIP is installed. You can refer to it to set or change PATH variable for example
rootPath=$1


############################### YOUR CODE STARTS HERE ###############################
## FOR GBPA ONLY!! ##
. /etc/profile.d/modules.sh
module load numpy pysam

# export PYTHONPATH where needed:
# export PYTHONPATH=/where/your/package/resides:${PYTHONPATH}
# for example:
# export PYTHONPATH=/apps/libs/numpy:$PYTHONPATH
# export PYTHONPATH=/apps/libs/pysam:$PYTHONPATH
# or simply load the corresponding modules if you use environment modules:
# module load numpy pysam

# adjust memory values for java heap memory (Xms and Xmx) and threads
export JAVA_Xms=1g
export JAVA_Xmx=32g
export JAVA_MaxPermSize=2g
# number of data threads
export JAVA_nt=4
# number of CPU threads
export JAVA_nct=8

# normally, you should no need to change the code below unless you want to use a different version of java, samtools and/or gatk
############################### YOUR CODE ENDS HERE ###############################

##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################



# adjust paths for shipped versions of required modules and packages

# jre 1.7.0_21
export JAVA_HOME=${rootPath}"/additionalFiles/jre1.7.0_21"
export JAVA_BINDIR=${rootPath}"/additionalFiles/jre1.7.0_21/bin"
export JAVA_TEMPDIR=${rootPath}"/additionalFiles/jre1.7.0_21/tmp"
export PATH=${rootPath}"/additionalFiles/jre1.7.0_21/bin":$PATH
export MANPATH=${rootPath}"/additionalFiles/jre1.7.0_21/man":$MANPATH


# samtools 0.1.18
export PATH=${rootPath}"/additionalFiles/samtools-0.1.18":${PATH}

# gatk 3.1.1
export PATH=${rootPath}"/additionalFiles/gatk-3.1.1/bin":${PATH}
export GATK_LIBRARY_PATH=${rootPath}"/additionalFiles/gatk-3.1.1/lib"


# perform some sanity checks (please do not remove!):
echo "-- performing some sanity checks --"
python -V > /dev/null 2>&1 || ( echo "There is no python executable!!" && exit 255 )
which java > /dev/null 2>&1 || ( echo "There is a problem with the java version (PATH=$PATH)" && exit 255 )
which samtools > /dev/null 2>&1 || ( echo "There is a problem with samtools executable (PATH=$PATH)" && exit 255 )
which gatk.sh > /dev/null 2>&1 || ( echo "There is a problem with gatk script (PATH=$PATH)" && exit 255 )
for mod in numpy pysam
do
    echo import $mod | python
    [[ $? -ne 0 ]] && echo "There is a problem with python module $mod" && exit 255
done
echo "-- done --"