#!/bin/bash

rootPath=$6
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

gold_indels1=${rootPath}"/additionalFiles/Mills_and_1000G_gold_standard.indels.b37.sites.vcf"
gold_indels2=${rootPath}"/additionalFiles/1000G_phase1.indels.b37.vcf"

# Options
reference=$1
tumor=$2
normal=$3
output_bam=$4
tar_ival=$5

# perform some sanity checks
filesToCheck="gold_indels1 gold_indels2 reference tumor normal"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done


# Run gatk indel
gatk.sh -T RealignerTargetCreator \
-o ${tar_ival} \
-I ${tumor} \
-I ${normal} \
-R ${reference} \
-known ${gold_indels1} \
-known ${gold_indels2} \
-nt ${JAVA_nt}

[[ $? -ne 0 ]] && exit 1

gatk.sh -T IndelRealigner \
-o ${output_bam} \
-I ${tumor} \
-I ${normal} \
-R ${reference} \
-known ${gold_indels1} \
-known ${gold_indels2} \
-targetIntervals ${tar_ival}