#!/bin/bash

rootPath=$5
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

gold_indels1=${rootPath}"/additionalFiles/Mills_and_1000G_gold_standard.indels.b37.sites.vcf"
gold_indels2=${rootPath}"/additionalFiles/1000G_phase1.indels.b37.vcf"
gold_cosmic=${rootPath}"/additionalFiles/b37_cosmic_v54_120711.vcf"
gold_dbsnp=${rootPath}"/additionalFiles/dbSnp137-00-All.vcf"

# Options
reference=$1
bam=$2
output_bam=$3
recal_table=$4

# perform some sanity checks
filesToCheck="gold_indels1 gold_indels2 gold_cosmic gold_dbsnp reference bam"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done


#Analyze patterns of covariation in the sequence dataset
gatk.sh -T BaseRecalibrator -R ${reference} -I ${bam} -knownSites ${gold_indels1}  -knownSites ${gold_indels2} -knownSites ${gold_cosmic} -knownSites ${gold_dbsnp} -o ${recal_table} -nct ${JAVA_nct}

#Do a second pass to analyze covariation remaining after recalibration
gatk.sh -T BaseRecalibrator -R ${reference} -I ${bam} -knownSites ${gold_indels1}  -knownSites ${gold_indels2} -knownSites ${gold_cosmic} -knownSites ${gold_dbsnp} -BQSR ${recal_table} -o ${recal_table} -nct ${JAVA_nct}

#Apply the recalibration to your sequence data
gatk.sh -T PrintReads -R ${reference} -I ${bam} -BQSR ${recal_table} -o ${output_bam} -nct ${JAVA_nct}
