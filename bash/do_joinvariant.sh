#!/bin/bash

rootPath=$4
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

# options
script=$1
vcfs=$2
outfile=$3

# perform some sanity checks
filesToCheck="script"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done


python ${script} --vcfs ${vcfs} --outfile ${outfile}