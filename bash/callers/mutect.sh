#!/bin/bash

rootPath=$6
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

gold_cosmic=${rootPath}"/additionalFiles/b37_cosmic_v54_120711.vcf"
gold_dbsnp=${rootPath}"/additionalFiles/dbSnp137-00-All.vcf"
# if you want to use a capture file, please uncomment the following two lines and adjust the first one with the corresponding capture file path
#capture_file=${rootPath}"/additionalFiles/SeqCap_EZ_Exome_v3_primary.slop50.g1k.bed"
#capture_file_param="-L ${capture_file}"

# Variables
normal=$1
tumor=$2
reference_fa=$3
prefix=$4
outdir=$5

output_vcf=${outdir}"/"${prefix}".mutect.snv.som.raw.vcf"

# perform some sanity checks
filesToCheck="gold_cosmic gold_dbsnp capture_file normal tumor reference_fa"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done


# we need jre 1.6.0_23
# it is not recommended to run with -nt param
export JAVA_HOME=${rootPath}"/callers/muTect-1.1.4/jre1.6.0_23"
export JAVA_BINDIR=${rootPath}"/callers/muTect-1.1.4/jre1.6.0_23/bin"
export JAVA_TEMPDIR=${rootPath}"/callers/muTect-1.1.4/jre1.6.0_23/tmp"
export PATH=${rootPath}"/callers/muTect-1.1.4/jre1.6.0_23/bin":$PATH
export MANPATH=${rootPath}"/callers/muTect-1.1.4/jre1.6.0_23/man":$MANPATH
java -Xmx${JAVA_Xmx} -jar ${rootPath}"/callers/muTect-1.1.4/muTect-1.1.4.jar" \
--analysis_type MuTect ${capture_file_param} \
--reference_sequence ${reference_fa} \
--input_file:normal ${normal} \
--input_file:tumor ${tumor} \
--cosmic ${gold_cosmic} \
--dbsnp ${gold_dbsnp} \
--vcf ${output_vcf}