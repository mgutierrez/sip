#!/bin/bash

rootPath=$6
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

# parameters
normal_bam=$1
tumor_bam=$2
reference_fa=$3
prefix=$4
outdir=$5

output_vcf=${outdir}"/"${prefix}".somaticsniper.snv.all.raw.vcf"

# perform some sanity checks
filesToCheck="normal_bam tumor_bam reference_fa"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done

${rootPath}"/callers/somatic-sniper-1.0.3/bam-somaticsniper" -J -F vcf -f ${reference_fa} ${tumor_bam} ${normal_bam} ${output_vcf}