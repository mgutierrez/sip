#!/bin/bash

rootPath=$6
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

# parameters
normal_bam=$1
tumor_bam=$2
reference_fa=$3
prefix=$4
outdir=$5

# perform some sanity checks
filesToCheck="normal_bam tumor_bam reference_fa"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done

java -Xms${JAVA_Xms} -Xmx${JAVA_Xmx} -jar ${rootPath}"/callers/varscan-2.3.6/VarScan.v2.3.6.jar" somatic \
<( samtools mpileup -f ${reference_fa} ${normal_bam} ${tumor_bam} ) \
${outdir}"/"${prefix} --mpileup 1 --output-vcf

mv ${outdir}"/"${prefix}".snp.vcf" ${outdir}"/"${prefix}".varscan2.snv.all.raw.vcf"
mv ${outdir}"/"${prefix}".indel.vcf" ${outdir}"/"${prefix}".varscan2.indel.all.raw.vcf"