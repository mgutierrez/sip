#!/bin/bash

rootPath=$6
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

# parameters
normal_bam=$1
tumor_bam=$2
reference_fa=$3
prefix=$4
outdir=$5

# perform some sanity checks
filesToCheck="normal_bam tumor_bam reference_fa"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done

# virmid discourages to use -t parameter
java -Xms${JAVA_Xms} -Xmx${JAVA_Xmx} -jar ${rootPath}"/callers/virmid-1.1.0/Virmid.jar" -R ${reference_fa} -D ${tumor_bam} -N ${normal_bam}

mv ${tumor_bam}".virmid.som.all.vcf" ${outdir}"/"${prefix}".virmid.snv.som.raw.vcf"
mv ${tumor_bam}".virmid.loh.all.vcf" ${outdir}"/"${prefix}".virmid.snv.loh.raw.vcf"
mv ${tumor_bam}".virmid.germ.all.vcf" ${outdir}"/"${prefix}".virmid.snv.germ.raw.vcf"

otherdir=${outdir}"/other"
mkdir -p ${otherdir}
mv ${tumor_bam}".virmid.som.passed.vcf" ${otherdir}
mv ${tumor_bam}".virmid.loh.passed.vcf" ${otherdir}
mv ${tumor_bam}".virmid.germ.passed.vcf" ${otherdir}
mv ${tumor_bam}".virmid.report" ${otherdir}