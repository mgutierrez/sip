#!/bin/bash

rootPath=$5
source ${rootPath}"/bash/loadModules.sh" ${rootPath}

# Variables
vcfs=$1
callers=$2
outfile=$3
script=$4

# perform some sanity checks
filesToCheck="script"
for f in $filesToCheck
do
	stat ${!f} >/dev/null 2>&1|| ( echo "File ${!f} does not exist!!" && exit 255 )
done


echo python ${script} -v ${vcfs} -c ${callers} -o ${outfile}
# Run
python ${script} \
-v ${vcfs} \
-c ${callers} \
-o ${outfile}