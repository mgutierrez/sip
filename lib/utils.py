'''
Created on 02/11/2013

@author: antonior
@author: Eva
'''
import logging, os

logger = logging.getLogger('root')

def make_dir(path):
    'It creates a directory if it does not exist'
    if not os.path.exists(path):
        logger.debug("Creating directory '%s'" % str(path))
        os.makedirs(path)
    else:
        logger.debug("Already created directory (skipping) '%s'" % str(path))
    return
