import logging,subprocess
logger = logging.getLogger('root')

def create_qsub_cmd(command, job_id, hold_jid=None, h_vmem='1G', path_stdout=None, path_stderr=None, pe_name=None, ncores=1):
    """
    creates the necessary qsub command from a normal command to submit to queue system
    :param command: command to submit
    :param job_id: id for submitted job
    :param hold_jid: list of hold id's
    :param h_vmem: h_vmem to reserve
    :param path_stdout: path to standard output stream
    :param path_stderr: path to error output stream
    :return: the qsub command ready to execute
    """
    if pe_name is None: raise StandardError("Unspecified parallel environment name! Aborting!")
    qsub = ' '.join(["qsub -N", job_id, "-pe", pe_name, str(ncores), "-l h_vmem="])+h_vmem

    if hold_jid != None:

        if type(hold_jid) == str:
            qsub = ' '.join([qsub, "-hold_jid", hold_jid])

        else:
            hold = ' '.join(["-hold_jid " + jid for jid in hold_jid])
            qsub = ' '.join([qsub, hold])

    if path_stdout != None:
        qsub = ' '.join([qsub, "-o", path_stdout])

    if path_stderr != None:
        qsub = ' '.join([qsub, "-e", path_stderr])

    qsub = ' '.join([qsub, command])

    return qsub


def runcmd(command):
    """
    run the specified command in a shell
    :param command: command string to execute
    :return: nothing
    """
    logger.debug("Running command '%s'" % str(command))
    # Checks whether an error occurred during the execution of the system call
    if subprocess.call(command.split()) != 0:
        logger.critical("Error while running command '%s'" % command)
    return




