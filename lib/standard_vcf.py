#!/usr/bin/python
'''
This module converts an vcf file from a certain caller to a standard version defined by GBPA
The currently supported callers are: virmid, somaticsniper, mutect and varscan2
You can add a non-supported caller by implementing a new class that extends "vcfFromCaller" (instructions and examples below)
The resulting vcf has the following variant format:

#CHROM	        POS	        ID	    REF	            ALT	            QUAL	    FILTER	    INFO	FORMAT	    NORMAL	            TUMOR
<chromosome>    <position>  <id>    <reference>     <alternative>   <quality>   <filter>    <info>  <format>    <normal_sample>     <tumor_sample>

Fields, python attributes and types of variant object:
1  - chromosome: chromosome identifier. Type: string. Field name: chrom
2  - position: position within the chromosome. Type: int. Field name: pos
3  - id: variant identifier. Type: string. Field name: id
4  - reference: reference nucleotide. Type: string. Field name: ref
5  - alternative: alternative nucleotide. Type: string. Field name: alt
6  - quality: quality value (float or '.' dot character). Type: string. Field name: qual
7  - filter: semicolon separated filters list (prepended prefix). Type: string. Field name: filter
8  - info: semicolon separated info list (prepended prefix). Type: dict. Field name: info
9  - format: format of the following normal and tumor samples (prepended prefix). Type: list. Field name: format
10 - normal_sample: normal sample values    |
11 - tumor_sample: tumor sample values      |   -----> normal and tumor samples are saved into a dict named 'genoinfo', where keys (NORMAL and TUMOR)
                                                        are dicts as well, which keys are the format values

therefore, if you want to implement a new caller please ensure that the variant you are returning follows the format described above.

For example, this is an original mutect variant:
CHROM   POS     ID	        REF ALT	QUAL	FILTER	    INFO	FORMAT	                    EXO_30	                EXO_29
1	    14907	rs79585140	A	G	.	    REJECT	    DB	    GT:AD:BQ:DP:FA	            0/1:41,24:33:65:0.369	0:39,18:.:57:0.316

this would be the python representation:
variant.chrom = '1'
variant.pos = 14907
variant.id = 'rs79585140'
variant.ref = 'A'
variant.alt = 'G'
variant.qual = '.'
variant.filter = 'REJECT'
variant.info = {'DB': ''}
variant.format = ['GT', 'FA', 'AD', 'DP', 'BQ']
variant.genoinfo = {'EXO_29': {'FA': '0.316', 'GT': '0', 'AD': '39,18', 'DP': '57', 'BQ': '.'}, 'EXO_30': {'FA': '0.369', 'GT': '0/1', 'AD': '41,24', 'DP': '65', 'BQ': '33'}}

And this would be the same standardized variant:
CHROM   POS     ID	        REF ALT	QUAL	FILTER	    INFO	FORMAT	                    NORMAL	                TUMOR
1	    14907	rs79585140	A	G	.	    mtREJECT	mtDB	mtGT:mtAD:mtBQ:mtDP:mtFA	0/1:41,24:33:65:0.369	0:39,18:.:57:0.316

and the python representation:
variant.chrom = '1'
variant.pos = 14907
variant.id = 'rs79585140'
variant.ref = 'A'
variant.alt = 'G'
variant.qual = '.'
variant.filter = 'mtREJECT'
variant.info = {'mtDB': ''}
variant.format = ['mtGT', 'mtFA', 'mtAD', 'mtDP', 'mtBQ']
variant.genoinfo = {'TUMOR': {'mtDP': '57', 'mtBQ': '.', 'mtAD': '39,18', 'mtGT': '0/0', 'mtFA': '0.316'}, 'NORMAL': {'mtDP': '65', 'mtBQ': '33', 'mtAD': '41,24', 'mtGT': '0/1', 'mtFA': '0.369'}}


Please note that some fields now have prepended the prefix of the caller. The prefix is set in the implementation of the new caller class. Also note
that in this case EXO_30 and EXO_29 have been renamed to NORMAL and TUMOR. This is mandatory.
Some callers (virmid for example) does not provide any genotype information. It's up to you to build that info.

'''
from abc import ABCMeta, abstractmethod
from GbpaCommonLibs.FileModels.vcf_file import vcf_file
from GbpaCommonLibs.DataModels.variant import variant

'''
This is the template to implement a new caller
Please do not forget the underscore in the name
Provide MYCALLER and MYPREFIX appropriated values and implement getNormalizedVcf method

class vcfFromCaller_MYCALLER(VcfFromCaller, object):
# do not modify __init__ method except PREFIX value
def __init__(self,filename,callerPrefix="MYPREFIX"):
    self.filename=filename
    self.prefix=callerPrefix
    super(self.__class__, self).__init__()

    # to implement (abstract method). **MUST** return an vcf_file object
    def getNormalizedVcf(self):
        def my_get_snv(position):
            # retrieve the variant from the original caller file
            variant=self.get_snv(position)
            # perform here desired changes to variant
            ...
            # do not forget to normalize (see method documentation) the variant:
            variant.normalize(prefix=self.prefix)
            # these steps may be necessary, or not. Depends on caller:
            ...
            variant.sampleNames=["NORMAL","TUMOR"]
            ...
            # more changes?
            ...
            #
            return variant

        vcfFile=vcf_file(self.filename)
        # header (or other fields) change needed?
        ...
        #
        vcfFile.get_snv=my_get_snv

        return vcfFile
'''


class VcfFromCaller(vcf_file, object):
    ''' This is the abstract and main class
    All callers classes inherits from here
    Only getNormalizedVcf method *MUST* be implemented returning an vcf_file object
    You don't need to modify this class
    '''
    __metaclass__ = ABCMeta

    def __init__(self):
        super(VcfFromCaller, self).__init__(self.filename)


    def combine(self, vcfCallerObjectsList):
        """
        combines a standard vcf (self) with a list of standard vcfs
        :param vcfCallerObjectsList: list of vcfFromCaller_XXX objects to combine with 'self'
        :raise ValueError: raises a ValueError exception if 'quality' field format in any vcf is wrong
        :return: generator to iterate over all the snvs
        """
        vcfCallerObjectsList.append(self)

        # get all positions (chrom/id tuple) from all the callers
        all_positions = set()
        for reader in vcfCallerObjectsList:
            reader._make_index()
            all_positions = set(reader._index).union(all_positions)

        # all_positions is a set (unsorted). Let's convert to a sorted list for a better output
        all_positionsList = list(all_positions)
        all_positionsList.sort(key=lambda t: variant(t[0], t[1]))

        # iterate over all positions
        for pos in all_positionsList:
            # for every position create a variable to save the new line
            # create a new and empty variant with only chrom/id values (position) and fill it with combined data
            newline=variant(pos[0], pos[1], filter_l='', id='.', ref='', alt='', samplenames=["NORMAL", "TUMOR"])
            newline.format = []
            newline.genoInfo = {"NORMAL": {}, "TUMOR": {}}

            # for every caller look for that position
            for reader in vcfCallerObjectsList:
                # if a caller reports a position save the info
                try:
                    snv = reader.getNormalizedVcf().get_snv(pos)
                # If a caller does not report a position continue without doing anything
                except KeyError:
                    continue

                # modify necessary fields
                newline.info = dict(newline.info.items()+snv.info.items())
                newline.format.extend(snv.format)
                newline.filter = snv.filter if newline.filter == '' else ";".join([newline.filter,snv.filter])
                newline.genoInfo["NORMAL"] = dict(newline.genoInfo["NORMAL"].items() + snv.genoInfo["NORMAL"].items())
                newline.genoInfo["TUMOR"] = dict(newline.genoInfo["TUMOR"].items() + snv.genoInfo["TUMOR"].items())
                # If some of these keys are not in newline take the value from snv
                newline.id = snv.id if newline.id == "." else newline.id
                newline.ref = snv.ref if newline.ref == "" else newline.ref
                if newline.qual == '.':
                    newline.qual = snv.qual
                elif snv.qual != ".":
                    try:
                        newline.qual = max(float(newline.qual), float(snv.qual))
                    except ValueError:
                        raise ValueError("quality format error!: '%s' or '%s'" % (newline.qual, snv.qual))
                # If newline has the ALT field, it compares both fields from newline and snv
                if newline.alt != "":
                    alt1 = set(newline.alt.split(','))
                    alt2 = set(snv.alt.split(','))
                    alt1 = list(alt1.union(alt2))

                    newline.alt = ','.join(alt1)
                else:
                    newline.alt = snv.alt


            yield newline

    @abstractmethod
    def getNormalizedVcf(self):
        pass


# class to define mutect caller
class VcfFromCaller_mutect(VcfFromCaller, object):
    """ mutect class
    """
    # do not modify __init__ method except PREFIX value
    # we decide to set prefix to 'mt' (mutect)
    def __init__(self, filename, callerPrefix="mt"):
        self.filename = filename
        self.prefix = callerPrefix
        super(self.__class__, self).__init__()

    # implementation of the abstract method
    def getNormalizedVcf(self):
        def my_get_snv(position):
            # retrieve the variant from the original caller file
            variant = self.get_snv(position)
            # in mutect we need to change the last two columns to NORMAL and TUMOR
            variant.genoInfo["NORMAL"] = variant.genoInfo.pop(self.header['colnames'][9])
            variant.genoInfo["TUMOR"] = variant.genoInfo.pop(self.header['colnames'][10])
            # also the GT value has to be fixed
            if variant.genoInfo["NORMAL"]["GT"] == "0": variant.genoInfo["NORMAL"]["GT"] = "0/0"
            if variant.genoInfo["TUMOR"]["GT"] == "0": variant.genoInfo["TUMOR"]["GT"] = "0/0"
            # please do not forget to normalize the variant. Refer to 'normalize' method documentation for more info
            variant.normalize(prefix=self.prefix)
            # sample names have to be changed to NORMAL and TUMOR
            variant.sampleNames = ["NORMAL", "TUMOR"]
            return variant

        vcfFile = vcf_file(self.filename)
        # in mutect, the header colnames must be changed to NORMAL and TUMOR
        vcfFile.header['colnames'][9:]=["NORMAL", "TUMOR"]
        vcfFile.get_snv = my_get_snv

        return vcfFile

# class to define virmid caller
class VcfFromCaller_virmid(VcfFromCaller, object):
    ''' virmid class
    '''
    # do not modify __init__ method except PREFIX value
    # we decide to set prefix to 'vm' (virmid)
    def __init__(self, filename, callerPrefix="vm"):
        self.filename = filename
        self.prefix = callerPrefix
        super(self.__class__, self).__init__()

    # implementation of the abstract method
    def getNormalizedVcf(self):
        def my_get_snv(position):
            variant = self.get_snv(position)
            # in virmid, the 'format' column is missing, but we must define it
            variant.format = []
            # virmid does not provide GT info. Let's find it out (and somatic status as well):
            variant.genoInfo = {"NORMAL": {}, "TUMOR": {}}
            pref = variant.info['gt'][0:2]
            if pref == 'lh':
                gtNormal = "0/1"
                gtTumor = "0/0"
                ss = "3"
            elif pref == 'lH':
                gtNormal = "1/1"
                gtTumor = "0/0"
                ss = "3"
            elif pref == 'hs':
                gtNormal = "0/0"
                gtTumor = "0/1"
                ss = "2"
            elif pref == 'Hs':
                gtNormal = "0/0"
                gtTumor = "1/1"
                ss = "2"
            elif pref == 'H,':
                gtNormal = "1/1"
                gtTumor = "1/1"
                ss = "1"
            elif pref == 'h,':
                gtNormal = "0/1"
                gtTumor = "0/1"
                ss = "1"
            else:
                raise StandardError('virmid genotype not recognized: %s' % pref)

            variant.genoInfo['NORMAL']['GT'] = gtNormal
            variant.genoInfo['TUMOR']['GT'] = gtTumor
            variant.format.append("GT")
            variant.info['SS'] = ss
            # additional modification exclusive for virmid
            if float(variant.info['gt'][2:]) < 0.9: variant.filter = "REJECT"
            if not "PASS" in variant.filter: variant.filter = "REJECT"
            # finally, let's normalize it, as usual
            variant.normalize(prefix=self.prefix)
            return variant

        vcfFile = vcf_file(self.filename)
        # and change colnames, like we did in mutect
        vcfFile.header['colnames'][9:] = ["NORMAL", "TUMOR"]
        vcfFile.get_snv = my_get_snv

        return vcfFile

# class to define varscan2 caller
class VcfFromCaller_varscan2(VcfFromCaller, object):
    ''' varscan2 class
    '''
    # do not modify __init__ method except PREFIX value
    # we decide to set prefix to 'vs' (varscan2)
    def __init__(self, filename, callerPrefix="vs"):
        self.filename = filename
        self.prefix = callerPrefix
        super(self.__class__, self).__init__()

    # implementation of the abstract method
    def getNormalizedVcf(self):
        def my_get_snv(position):
            # varscan2 caller does not need almost any modification, only normalization. This is a very easy caller :-)
            variant = self.get_snv(position)
            variant.normalize(prefix=self.prefix)
            return variant

        vcfFile = vcf_file(self.filename)
        vcfFile.get_snv = my_get_snv

        return vcfFile


# class to define somaticscniper caller
class VcfFromCaller_somaticsniper(VcfFromCaller, object):
    ''' somaticsniper class
    '''
    # do not modify __init__ method except PREFIX value
    # we decide to set prefix to 'ss' (somaticsniper)
    def __init__(self, filename, callerPrefix="ss"):
        self.filename = filename
        self.prefix = callerPrefix
        super(self.__class__, self).__init__()


    # implementation of the abstract method
    def getNormalizedVcf(self):
        def my_get_snv(position):
            # this is a very easy implementation as well. Let's only normalize it
            variant = self.get_snv(position)
            variant.normalize(prefix=self.prefix)
            return variant

        vcfFile = vcf_file(self.filename)
        vcfFile.get_snv = my_get_snv

        return vcfFile

